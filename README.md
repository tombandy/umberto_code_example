#Umberto Code Example
====================

Some example code from a small project built last year.

The project was an advent calendar for Umberto Giannini. Written in AS3 and using the RobotLegs framework, Flint was also used for the particle effects. A freelance back-end developer wrote the php cms elements, these were used by the RL service to show prize information.