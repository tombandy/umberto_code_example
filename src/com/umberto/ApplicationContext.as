package com.umberto {
	import com.umberto.controller.GetPrizeCommand;
	import com.umberto.controller.GetStatusCommand;
	import com.umberto.controller.InitialiseApplicationStateCommand;
	import com.umberto.controller.InitialiseCmsServiceCommand;
	import com.umberto.controller.LoadConfigCommand;
	import com.umberto.controller.SetCurrentPrizeCommand;
	import com.umberto.controller.SubmitPrizeCommand;
	import com.umberto.events.CmsServiceEvent;
	import com.umberto.events.ConfigServiceEvent;
	import com.umberto.events.CustomEvent;
	import com.umberto.model.ApplicationStateModel;
	import com.umberto.model.IApplicationStateModel;
	import com.umberto.remote.services.CmsService;
	import com.umberto.remote.services.ConfigService;
	import com.umberto.remote.services.ICmsService;
	import com.umberto.remote.services.IConfigService;
	import com.umberto.view.BackgroundMediator;
	import com.umberto.view.CalendarMediator;
	import com.umberto.view.LoserMediator;
	import com.umberto.view.ParticleMediator;
	import com.umberto.view.WinnerMediator;
	import com.umberto.view.components.BackgroundView;
	import com.umberto.view.components.CalendarView;
	import com.umberto.view.components.LoserView;
	import com.umberto.view.components.ParticleView;
	import com.umberto.view.components.WinnerView;
	import com.umberto.view.components.ui.Door;

	import org.robotlegs.base.ContextEvent;
	import org.robotlegs.mvcs.Context;

	import flash.display.DisplayObjectContainer;

	public class ApplicationContext extends Context {
		public function ApplicationContext(contextView : DisplayObjectContainer) {
			super(contextView);
		}

		override public function startup() : void {
			commandMap.mapEvent(ContextEvent.STARTUP_COMPLETE, InitialiseApplicationStateCommand, ContextEvent, true);
			commandMap.mapEvent(ContextEvent.STARTUP_COMPLETE, LoadConfigCommand, ContextEvent, true);
			commandMap.mapEvent(ConfigServiceEvent.SERVICE_READY, InitialiseCmsServiceCommand, ConfigServiceEvent, true);
			commandMap.mapEvent(CmsServiceEvent.SERVICE_READY, GetStatusCommand, CmsServiceEvent, true);
			commandMap.mapEvent(Door.OPEN, GetPrizeCommand, CustomEvent);
			commandMap.mapEvent(CmsServiceEvent.SERVICE_PRIZE, SetCurrentPrizeCommand, CmsServiceEvent);
			commandMap.mapEvent(WinnerView.SEND, SubmitPrizeCommand, CustomEvent);

			injector.mapSingletonOf(IConfigService, ConfigService);
			injector.mapSingletonOf(ICmsService, CmsService);
			injector.mapSingletonOf(IApplicationStateModel, ApplicationStateModel);

			mediatorMap.mapView(BackgroundView, BackgroundMediator);
			mediatorMap.mapView(CalendarView, CalendarMediator);
			mediatorMap.mapView(WinnerView, WinnerMediator);
			mediatorMap.mapView(LoserView, LoserMediator);
			mediatorMap.mapView(ParticleView, ParticleMediator);

			contextView.addChild(new BackgroundView());
			contextView.addChild(new CalendarView());
			contextView.addChild(new WinnerView());
			contextView.addChild(new LoserView());
			contextView.addChild(new ParticleView());

			super.startup();
		}
	}
}