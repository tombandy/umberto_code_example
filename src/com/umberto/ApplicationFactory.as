package com.umberto {
	import com.greensock.plugins.AutoAlphaPlugin;
	import com.greensock.plugins.TweenPlugin;
	import com.umberto.view.components.ui.Preloader;
	import com.umberto.view.components.ui.PreloaderNums;

	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.system.Security;
	import flash.utils.getDefinitionByName;

	/**
	 * @author Tom
	 */
	public class ApplicationFactory extends MovieClip {
		private var _preloader : PreloaderNums;

		public function ApplicationFactory() {
			trace("Umberto Giannini Advent Calendar 2012. Build date ", Version.BUILD_DATE, " ver ", Version.BUILD_NUMBER);
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
//			Security.loadPolicyFile("http://profile.ak.fbcdn.net/crossdomain.xml");
			Security.allowDomain("*");
			Security.allowInsecureDomain("*");
			
			TweenPlugin.activate([AutoAlphaPlugin]);
			
//			_preloader = new Preloader(0xEAEBFA,0xA7A8BF);
			_preloader = new PreloaderNums();
			_preloader.addEventListener(Preloader.COMPLETE, onPreloaderComplete);
			_preloader.x = (stage.stageWidth-_preloader.width) / 2;
			_preloader.y = (stage.stageHeight-_preloader.height) / 2;
			addChild(_preloader);
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		private function onPreloaderComplete(e:Event):void{
			nextFrame();
			init();
		}

		public function onEnterFrame(event : Event) : void {
			graphics.clear();
			if (framesLoaded == totalFrames) {
				removeEventListener(Event.ENTER_FRAME, onEnterFrame);
				_preloader.update(100,100);
			} else {
				_preloader.update(root.loaderInfo.bytesLoaded, root.loaderInfo.bytesTotal);
			}
		}

		private function init() : void {
			var mainClass : Class = Class(getDefinitionByName("com.umberto.Application"));
			if (mainClass) {
				removeChild(_preloader);
				var app : Object = new mainClass(this);
				addChild(app as DisplayObject);
			} else {
				trace("[ERROR] ApplicationFactory couldn't find main class");
			}
		}
	}
}

