package com.umberto.model {
	import com.umberto.model.vo.PrizeVO;
	/**
	 * @author tombandy
	 */
	public interface IApplicationStateModel {
		function get currentPrize():PrizeVO;
		function set currentPrize(v:PrizeVO):void;
	}
}
