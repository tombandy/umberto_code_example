package com.umberto.model {
	import com.umberto.model.vo.PrizeVO;

	import org.robotlegs.mvcs.Actor;

	/**
	 * @author Tom
	 */
	public class ApplicationStateModel extends Actor implements IApplicationStateModel {
		
		private var _currentPrize:PrizeVO;
		
		public function ApplicationStateModel() {
		}
		
		public function get currentPrize():PrizeVO{return _currentPrize;}
		public function set currentPrize(v:PrizeVO):void{_currentPrize = v;}

	}
}
