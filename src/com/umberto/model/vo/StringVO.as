package com.umberto.model.vo {
	/**
	 * @author tom
	 */
	public class StringVO {
		public var id : String;
		public var value : String;

		public function StringVO(id : String = null, value : String = null) {
			this.id = id;
			this.value = value;
		}

		// TRACE
		public function toString() : String {
			return "StringVO: id=" + id + " value=" + value;
		}
	}
}
