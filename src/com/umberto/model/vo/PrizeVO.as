package com.umberto.model.vo {
	/**
	 * @author tom
	 */
	public class PrizeVO {
		
		public var prizeId:String;
		public var title:String;
		public var description:String;
		public var image:String;
		public var win:Boolean;
		public var buyLink:String;

		public function PrizeVO(prizeId : String = null) {
			this.prizeId = prizeId;
		}

		// TRACE
		public function toString() : String {
			return "PrizeVO: prizeId=" + prizeId;
		}
	}
}
