package com.umberto.model.vo {
	/**
	 * @author tom
	 */
	public class FormVO {
		
		public var prizeId:String;
		public var name:String;
		public var email:String;
		public var address1:String;
		public var address2:String;
		public var town:String;
		public var postcode:String;
		public var tnc:Boolean;

		public function FormVO(prizeId : String = null) {
			this.prizeId = prizeId;
		}

		// TRACE
		public function toString() : String {
			return "FormVO: prizeId=" + prizeId;
		}
	}
}
