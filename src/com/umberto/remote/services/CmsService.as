package com.umberto.remote.services {
	import com.umberto.Defines;
	import com.umberto.view.components.ui.Utils;
	import com.adobe.crypto.MD5;
	import com.umberto.events.CmsServiceEvent;
	import com.umberto.model.vo.FormVO;
	import com.umberto.model.vo.PrizeVO;

	import org.robotlegs.mvcs.Actor;

	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;

	/**
	 * @author tombandy
	 */
	public class CmsService extends Actor implements ICmsService {

		private var _serverUrl:String;
		
		public function CmsService() {
			
		}
		
		public function init(serverUrl:String):void{
			_serverUrl = serverUrl;
			dispatch(new CmsServiceEvent(CmsServiceEvent.SERVICE_READY));
		}

		public function getStatus():void{
			trace("CmsService.getStatus()");
			var request : URLRequest = new URLRequest(_serverUrl+"getstatus.php");
			request.method = URLRequestMethod.POST;
			var loader:URLLoader = new URLLoader();
			loader.dataFormat = URLLoaderDataFormat.TEXT;
			loader.addEventListener(Event.COMPLETE, onGetStatusComplete);
			try {
				loader.load(request);
			} catch (error : Error) {
				trace("Unable to call get prize.");
			}
		}

		private function onGetStatusComplete(e:Event):void{
			var result:Object = JSON.parse(e.target.data);
			trace("CmsService.onGetStatusComplete(e)",result.status,result.day);
			if(result.day<1||result.day>25){
				result.day = 1;
				result.status = 0;
			}
			if(result.status == 1)
				dispatch(new CmsServiceEvent(CmsServiceEvent.SERVICE_STATUS,result));
		}

		public function getPrize(door:String):void{
			trace("CmsService.getPrize(door)",door);
			var urlvars: URLVariables = new URLVariables;
			urlvars.door = door;
			urlvars.code = MD5.hash("h29Duys&2m"+door);
			var request : URLRequest = new URLRequest(_serverUrl+"getprize.php");
			request.method = URLRequestMethod.POST;
			request.data = urlvars;
			var loader:URLLoader = new URLLoader();
			loader.dataFormat = URLLoaderDataFormat.TEXT;
			loader.addEventListener(Event.COMPLETE, onGetPrizeComplete);
			try {
				loader.load(request);
			} catch (error : Error) {
				trace("Unable to call get prize.");
			}
		}

		private function onGetPrizeComplete(e:Event):void{
			var result:Object = JSON.parse(e.target.data);
			var prizeVO:PrizeVO = new PrizeVO(result.prize_id);
			prizeVO.title = result.title;
			prizeVO.description = result.description;
			prizeVO.image = result.image;
			prizeVO.win = (result.win == "1");
			prizeVO.buyLink = result.buy_link;
			trace("CmsService.onGetPrizeComplete(e)",prizeVO);
			dispatch(new CmsServiceEvent(CmsServiceEvent.SERVICE_PRIZE,prizeVO));
		}
		
		public function submitPrize(formVO:FormVO):void{
			trace("CmsService.submitPrize(formVO)",formVO);
			var urlvars: URLVariables = new URLVariables;
			urlvars.prize_id = formVO.prizeId;
			urlvars.name = formVO.name;
			urlvars.email = formVO.email;
			urlvars.address1 = formVO.address1;
			urlvars.address2 = formVO.address2;
			urlvars.town = formVO.town;
			urlvars.postcode = formVO.postcode;
			urlvars.tnc = formVO.tnc;
			urlvars.code = MD5.hash(Utils.getHistogram(Defines.COLOUR_DARK)+formVO.email);
			var request : URLRequest = new URLRequest(_serverUrl+"prizesubmit.php");
			request.method = URLRequestMethod.POST;
			request.data = urlvars;
			var loader:URLLoader = new URLLoader();
			loader.dataFormat = URLLoaderDataFormat.TEXT;
			loader.addEventListener(Event.COMPLETE, onSubmitPrizeComplete);
			try {
				loader.load(request);
			} catch (error : Error) {
				trace("Unable to call get prize.");
			}
		}

		private function onSubmitPrizeComplete(e:Event):void{
			trace("CmsService.onSubmitPrizeComplete(e)",e.target.data);
			var result:Object = JSON.parse(e.target.data);
			trace("CmsService.onSubmitPrizeComplete(e)",result.status);
			dispatch(new CmsServiceEvent(CmsServiceEvent.SERVICE_SUBMITTED));
		}
	}
}
