package com.umberto.remote.services {
	import com.umberto.events.ConfigServiceEvent;
	import com.umberto.model.vo.StringVO;

	import org.robotlegs.mvcs.Actor;

	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;

	/**
	 * @author tombandy
	 */
	public class ConfigService extends Actor implements IConfigService {
		private var _strings : Array;
		private var _competitionWeekVO : Array;

		public function ConfigService() {
			
		}

		public function load(flashvars : Object) : void {
			var loader : URLLoader = new URLLoader();
			loader.dataFormat = URLLoaderDataFormat.TEXT;
			loader.addEventListener(Event.COMPLETE, completeHandler);
			loader.addEventListener(Event.OPEN, openHandler);
			loader.addEventListener(ProgressEvent.PROGRESS, progressHandler);
			loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			loader.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);
			loader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);

			var request : URLRequest = new URLRequest(flashvars.configFile+"?t="+new Date().getTime());
			try {
				loader.load(request);
			} catch (error : Error) {
				trace("Unable to load requested document.");
			}
		}

		private function completeHandler(e : Event) : void {
			trace("ConfigService.completeHandler(e)");
			var xml : XML = new XML(URLLoader(e.target).data);
			var strings : XMLList = xml.strings.string;
			_strings = [];
			for (var i : int;i < strings.length();i++) {
				var configVO : StringVO = new StringVO(strings[i].@id, strings[i].text());
				_strings.push(configVO);
			}

			dispatch(new ConfigServiceEvent(ConfigServiceEvent.SERVICE_READY));
		}

		private function openHandler(event : Event) : void {
			trace("openHandler: " + event);
		}

		private function progressHandler(event : ProgressEvent) : void {
			trace("progressHandler loaded:" + event.bytesLoaded + " total: " + event.bytesTotal);
		}

		private function securityErrorHandler(event : SecurityErrorEvent) : void {
			trace("securityErrorHandler: " + event);
		}

		private function httpStatusHandler(event : HTTPStatusEvent) : void {
			trace("httpStatusHandler: " + event);
		}

		private function ioErrorHandler(event : IOErrorEvent) : void {
			trace("ioErrorHandler: " + event);
		}

		public function getString(id : String) : String {
			var result : StringVO = new StringVO();
			for (var i : int;i < _strings.length;i++) {
				if (_strings[i].id == id) {
					result = _strings[i];
				}
			}
			return result.value;
		}

		public function get competitionPrizes() : Array {
			return _competitionWeekVO;
		}
	}
}
