package com.umberto.remote.services {
	import com.umberto.model.vo.FormVO;
	/**
	 * @author tombandy
	 */
	public interface ICmsService {
		function init(serverUrl:String):void;
		function getStatus():void;
		function getPrize(door:String):void;
		function submitPrize(formVO:FormVO):void;
	}
}
