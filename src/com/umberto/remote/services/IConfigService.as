package com.umberto.remote.services {
	/**
	 * @author tombandy
	 */
	public interface IConfigService {
		function load(flashVars : Object) : void;

		function getString(id : String) : String;

		function get competitionPrizes() : Array;
	}
}
