package com.umberto.controller {
	import com.umberto.remote.services.IConfigService;

	import org.robotlegs.mvcs.Command;

	public class LoadConfigCommand extends Command {
		// [Inject]
		// public var event:ConfigServiceEvent;
		[Inject]
		public var service : IConfigService;

		override public function execute() : void {
			service.load(contextView.stage.loaderInfo.parameters);
		}
	}
}