package com.umberto.controller {
	import com.umberto.model.IApplicationStateModel;

	import org.robotlegs.mvcs.Command;

	public class InitialiseApplicationStateCommand extends Command {
		[Inject]
		public var applicationStateModel : IApplicationStateModel;

		override public function execute() : void {
			trace("InitialiseApplicationStateCommand.execute()");
		}
	}
}