package com.umberto.controller {
	import com.umberto.events.CustomEvent;
	import com.umberto.remote.services.ICmsService;

	import org.robotlegs.mvcs.Command;

	public class GetPrizeCommand extends Command {
		
		[Inject]
		public var event:CustomEvent;
		
		[Inject]
		public var cmsService : ICmsService;

		override public function execute() : void {
			cmsService.getPrize(event.params.door);
		}
	}
}