package com.umberto.controller {
	import com.umberto.remote.services.ICmsService;
	import com.umberto.remote.services.IConfigService;
	import org.robotlegs.mvcs.Command;

	public class InitialiseCmsServiceCommand extends Command {
		
		[Inject]
		public var configService:IConfigService;
		
		[Inject]
		public var cmsService : ICmsService;

		override public function execute() : void {
			cmsService.init(configService.getString("server_url"));
		}
	}
}