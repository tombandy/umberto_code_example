package com.umberto.controller {
	import com.umberto.events.CustomEvent;
	import com.umberto.remote.services.ICmsService;

	import org.robotlegs.mvcs.Command;

	public class SubmitPrizeCommand extends Command {
		
		[Inject]
		public var event:CustomEvent;
		
		[Inject]
		public var service : ICmsService;

		override public function execute() : void {
			service.submitPrize(event.params.formVO);
		}
	}
}