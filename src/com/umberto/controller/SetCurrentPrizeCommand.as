package com.umberto.controller {
	import com.umberto.events.CmsServiceEvent;
	import com.umberto.model.IApplicationStateModel;
	import com.umberto.model.vo.PrizeVO;

	import org.robotlegs.mvcs.Command;

	public class SetCurrentPrizeCommand extends Command {
		
		[Inject]
		public var event:CmsServiceEvent;
		
		[Inject]
		public var service : IApplicationStateModel;

		override public function execute() : void {
			service.currentPrize = event.data as PrizeVO;
		}
	}
}