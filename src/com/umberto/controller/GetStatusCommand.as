package com.umberto.controller {
	import com.umberto.remote.services.ICmsService;

	import org.robotlegs.mvcs.Command;

	public class GetStatusCommand extends Command {
		
		[Inject]
		public var cmsService : ICmsService;

		override public function execute() : void {
			cmsService.getStatus();
		}
	}
}