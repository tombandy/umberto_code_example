package com.umberto {
	import com.greensock.plugins.AutoAlphaPlugin;
	import com.greensock.plugins.TweenPlugin;

	import flash.display.Sprite;

	[SWF(width="1020", height="820", frameRate="31", backgroundColor="#000000")]
	[Frame(factoryClass="com.umberto.ApplicationFactory")]
	public class Application extends Sprite {
		private var _context : ApplicationContext;

		public function Application(appFactory : ApplicationFactory) {
			TweenPlugin.activate([AutoAlphaPlugin]);
			_context = new ApplicationContext(this);
		}
	}
}
