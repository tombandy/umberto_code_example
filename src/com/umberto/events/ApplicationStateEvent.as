package com.umberto.events {
	import flash.events.Event;

	public class ApplicationStateEvent extends Event {
		public static const UPDATE_SCORE : String = "ApplicationStateEvent.UpdateScore";
		public var score : int;

		public function ApplicationStateEvent(type : String, score : int = 0) {
			super(type, true, true);
			this.score = score;
		}

		override public function clone() : Event {
			return new ApplicationStateEvent(this.type, this.score);
		}
	}
}