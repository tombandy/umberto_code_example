package com.umberto.events {
	
	import flash.events.Event;
	
	public class CustomEvent extends Event{
		
		private var _params:Object;
		
		public function CustomEvent(type:String, params:Object, bubbles:Boolean = false, cancelable:Boolean = false){
			super(type, bubbles, cancelable);
			_params = params;
		}
		
		public override function clone():Event{
			return new CustomEvent(type, _params, bubbles, cancelable);
		}
		
		public override function toString():String{
			return formatToString("CustomEvent", "params", "type", "bubbles", "cancelable");
		}
		
		public function get params():Object{return _params;}
	}
}