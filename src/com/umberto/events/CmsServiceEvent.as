package com.umberto.events {
	import flash.events.Event;

	public class CmsServiceEvent extends Event {
		
		public static const SERVICE_READY : String = "CMSServiceEvent.ServiceReady";
		public static const SERVICE_STATUS : String = "CMSServiceEvent.ServiceStatus";
		public static const SERVICE_PRIZE : String = "CMSServiceEvent.ServicePrize";
		public static const SERVICE_SUBMITTED : String = "CMSServiceEvent.ServiceSubmitted";
		
		public var data : Object;

		public function CmsServiceEvent(type : String, data : Object = null) {
			this.data = data;
			super(type, true, true);
		}

		override public function clone() : Event {
			return new ConfigServiceEvent(this.type, this.data);
		}
	}
}