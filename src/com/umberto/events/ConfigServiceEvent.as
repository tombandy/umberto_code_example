package com.umberto.events {
	import flash.events.Event;

	public class ConfigServiceEvent extends Event {
		public static const SERVICE_READY : String = "ConfigServiceEvent.ServiceReady";
		public var service : Object;

		public function ConfigServiceEvent(type : String, service : Object = null) {
			this.service = service;
			super(type, true, true);
		}

		override public function clone() : Event {
			return new ConfigServiceEvent(this.type, this.service);
		}
	}
}