package com.umberto.view {
	import com.umberto.events.CmsServiceEvent;
	import com.umberto.remote.services.IConfigService;
	import com.umberto.view.components.BackgroundView;
	import com.umberto.view.components.LoserView;
	import com.umberto.view.components.WinnerView;

	import org.robotlegs.mvcs.Mediator;

	import flash.events.Event;

	/**
	 * @author tombandy
	 */
	public class BackgroundMediator extends Mediator {
		
		[Inject]
		public var view:BackgroundView;
		
		[Inject]
		public var configService:IConfigService;
		
		public function BackgroundMediator() {
		}
		
		override public function onRegister():void{
			addContextListener(CmsServiceEvent.SERVICE_STATUS, onStatus);
			addContextListener(CmsServiceEvent.SERVICE_PRIZE, onPrize);
			addContextListener(WinnerView.BACK, onBack);
			addContextListener(LoserView.BACK, onBack);
			
			addViewListener(BackgroundView.SHOW_COMPLETE, dispatch, Event);
			addViewListener(BackgroundView.FACEBOOK_SHARE_CLICK, dispatch, Event);
			addViewListener(BackgroundView.TWITTER_SHARE_CLICK, dispatch, Event);
		}
		
		private function onStatus(e:CmsServiceEvent) : void {
			view.show();
		}
		
		private function onPrize(e:Event) : void {
			view.fadeToPrize();
		}
		
		private function onBack(e:Event) : void {
			view.fadeToCalendar();
		}
		
	}
}
