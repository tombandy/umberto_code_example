package com.umberto.view.components.ui {
	import com.greensock.TweenLite;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;

	/**
	 * @author Tom
	 */
	public class Preloader extends Sprite {
		
		public static const COMPLETE:String = "Preloader.Complete";
		
		private var _bgColour:uint;
		private var _fillColour:uint;
		private var _radius:Number;
		private var _percent:Number = 0;
		
		public function Preloader(bgColour:uint=0x000000, fillColour:uint=0xFFFFFF, radius:Number=20) {
			_bgColour = bgColour;
			_fillColour = fillColour;
			_radius = radius;
		}
		
		public function update(bytesLoaded:Number, bytesTotal:Number):void{
			var pct:Number = (bytesLoaded / bytesTotal) * 100;
			TweenLite.killTweensOf(this);
			TweenLite.to(this,1,{percent:pct,onComplete:onUpdateComplete});
		}
		
		private function draw():void{
			graphics.clear();
			graphics.beginFill(_bgColour);
			graphics.drawCircle(0, 0, _radius);
			graphics.endFill();
			graphics.beginFill(_fillColour);
			graphics.moveTo(0,0);
			
			var center:Point = new Point(0,0);
			
			var endAngle:Number = Math.round((_percent / 100) * 360);
			
			for(var i:int;i<=endAngle;i++){
				var radian:Number = ((i-90)/180)*Math.PI;
		        var pointX:Number = center.x+Math.cos(radian)*_radius;
		        var pointY:Number = center.y+Math.sin(radian)*_radius;
		        
		        graphics.lineTo(pointX, pointY);
		    }
		    graphics.endFill();
		}
		
		public function set percent(v:Number):void{
			_percent = v;
			draw();
		}
		
		public function get percent():Number{
			return _percent;
		}
		
		private function onUpdateComplete():void{
			if(_percent == 100){
				TweenLite.killTweensOf(this);
				TweenLite.to(this,.3,{alpha:0,onComplete:onFadeComplete});
			}
		}
		
		private function onFadeComplete():void{
			dispatchEvent(new Event(COMPLETE));
		}
	}
}
