package com.umberto.view.components.ui {
	import com.greensock.TweenLite;
	import com.greensock.easing.Linear;
	import com.umberto.events.CustomEvent;
	import com.umberto.view.components.ui.flint.DoorStarEmitter;

	import org.flintparticles.twoD.renderers.BitmapRenderer;

	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.IBitmapDrawable;
	import flash.display.MovieClip;
	import flash.display.PixelSnapping;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.PerspectiveProjection;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	/**
	 * @author Tom
	 */
	public class Door extends DoorAsset {
		
		public static const OPEN:String = "Door.Open";
		public static const OPENED:String = "Door.Opened";
		public static const SHOWN:String = "Door.Shown";
		public static const HIDDEN:String = "Door.Hidden";
		
		private var _id:uint;
		private var _flint : BitmapRenderer;
		private var _starEmitter:DoorStarEmitter;
		private var _doorBounds:Rectangle;
		
		public function Door(id:uint) {
			_id = id;
			stop();
			alpha = 0;
			visible = false;
			blendMode = BlendMode.LAYER;
			transform.perspectiveProjection = new PerspectiveProjection();
			transform.perspectiveProjection.projectionCenter = new Point(52, 56);
			
			_doorBounds = new Rectangle(0,0,door.width,door.height);			
			_flint = new BitmapRenderer(_doorBounds);
			_flint.mouseChildren = false;
			_flint.mouseEnabled = false;
//			_flint.addFilter( new BlurFilter( 4, 4, 1 ) );
//      	_flint.addFilter( new ColorMatrixFilter( [ 1,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0.95,0 ] ) );

			_starEmitter = new DoorStarEmitter(_flint);
			_starEmitter.x = door.width/2;
			_starEmitter.y = door.height/2;
			
			_flint.addEmitter(_starEmitter);
			
			particlesHolder.addChild(_flint);
			
			var overlay:Sprite = door.getChildByName("overlay") as Sprite;
			overlay.alpha = .1;
			
			var numberText:TextField = door.getChildByName("numberText") as TextField;
			numberText.text = String(_id+1);
			
			buttonMode = true;
			mouseChildren = false;
			mouseEnabled = true;
			addEventListener(MouseEvent.CLICK, onDoorClick);
			
		}
		
		public function setImage(sourceImage:IBitmapDrawable,loc:Point):void{
			var imageHolder:Sprite = door.getChildByName("imageHolder") as Sprite;
			var bmd:BitmapData = new BitmapData(door.width, door.height,false,0xFFFFFF);
			var m:Matrix = new Matrix();
			m.translate(-loc.x, -loc.y);
			bmd.draw(sourceImage,m,null,null,_doorBounds,true);
			var bmp:Bitmap = new Bitmap(bmd,PixelSnapping.AUTO,true);
			imageHolder.addChild(bmp);
		}
		
		private function onDoorClick(e:MouseEvent):void{
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
			gotoAndPlay(1);
			_starEmitter.start();
			dispatchEvent(new CustomEvent(OPEN, {"door":_id+1},true));
		}
		
		public function set isOpen(v:Boolean):void{
			mouseEnabled = v;
			var lock:MovieClip = door.getChildByName("lock") as MovieClip;
			lock.gotoAndStop(v?1:2);
		}
		
		private function onEnterFrame(e:Event):void{
			if(currentFrame==totalFrames){
				removeEventListener(Event.ENTER_FRAME, onEnterFrame);
				stop();
				dispatchEvent(new CustomEvent(OPENED, {"bounds":new Rectangle(this.x,this.y,_doorBounds.width,_doorBounds.height)},true));
				_starEmitter.counter.stop();
			}
		}
		
		public function reset():void{
			gotoAndStop(1);
			_starEmitter.stop();
			TweenLite.killTweensOf(this);
			alpha = 1;
			visible = true;
		}
		
		public function show(speed:Number=.5,delay:Number=0):void{
			alpha = 0;
			TweenLite.killTweensOf(this);
			TweenLite.to(this,speed,{autoAlpha:1,ease:Linear.easeNone,delay:delay,onComplete:showComplete});
		}
		
		private function showComplete():void{
			dispatchEvent(new Event(SHOWN));
		}
		
		public function hide(speed:Number=.5,delay:Number=0):void{
			TweenLite.killTweensOf(this);
			TweenLite.to(this,speed,{autoAlpha:0,ease:Linear.easeNone,delay:delay,onComplete:hideComplete});
		}
		
		private function hideComplete():void{
			dispatchEvent(new Event(HIDDEN));
		}
	}
}
