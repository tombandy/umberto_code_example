package com.umberto.view.components.ui {
	import flash.events.MouseEvent;
	import flash.display.MovieClip;
	import flash.events.EventDispatcher;

	/**
	 * @author tombandy
	 */
	public class SimpleButton extends EventDispatcher {
		
		private var _view:MovieClip;
		
		public function SimpleButton(view:MovieClip) {
			_view = view;
			_view.gotoAndStop(1);
			_view.buttonMode = true;
			_view.mouseChildren = false;
			_view.addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			_view.addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
			_view.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			_view.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}
		
		private function onMouseOver(e:MouseEvent):void{
			_view.gotoAndStop(2);
		}
		
		private function onMouseOut(e:MouseEvent):void{
			_view.gotoAndStop(1);
		}
		
		private function onMouseDown(e:MouseEvent):void{
			_view.gotoAndStop(3);
		}
		
		private function onMouseUp(e:MouseEvent):void{
			_view.gotoAndStop(2);
		}
	}
}
