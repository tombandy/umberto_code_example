package com.umberto.view.components.ui {
	import flash.events.KeyboardEvent;
	import flash.events.TimerEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.FocusEvent;
	import flash.text.TextField;
	import flash.utils.Timer;

	/**
	 * @author Tom
	 */
	public class VerySimpleInputTextField extends EventDispatcher {
		
		public static const FILLED_IN:String = "VerySimpleInputTextField.FilledIn";
		public static const CHANGE:String = "VerySimpleInputTextField.Change";
		
		private var _view:TextField;
		private var _defaultCopy:String;
		private var _warningCopy:String;
		private var _colourBeforeWarning:uint;
		private var _textBeforeWarning:String;
		private var _warningTimer:Timer;
		private var _isWarningShowing:Boolean = false;
		
		public function VerySimpleInputTextField(textField:TextField, defaultCopy:String, tabIndex:int=-1) {
			_view = textField;
			_defaultCopy = defaultCopy;
			if(tabIndex > -1)_view.tabIndex = tabIndex;
			
			_view.text = _defaultCopy;
			
			_view.addEventListener(FocusEvent.FOCUS_IN, onFocusIn);
			_view.addEventListener(FocusEvent.FOCUS_OUT, onFocusOut);
			_view.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
		}
		
		private function onFocusIn(e:FocusEvent):void{
			if(_isWarningShowing){
				_warningTimer.reset();
				_view.textColor = _colourBeforeWarning;
				_view.mouseEnabled = true;
				_isWarningShowing = false;
			}
			if(_view.text == _defaultCopy || _view.text == _warningCopy) {
				_view.text = "";
			}
		}
		
		private function onFocusOut(e:FocusEvent):void{
			if(_view.text == ""){
				_view.text = _defaultCopy;
			} else {
				dispatchEvent(new Event(FILLED_IN));
			}
		}
		
		private function onKeyUp(e:KeyboardEvent):void{
			dispatchEvent(new Event(CHANGE));
		}
		
		public function get isFilledIn():Boolean{
			return (_view.text == "" || _view.text == _defaultCopy || _view.text == _warningCopy) ? false : true;
		}
		
		public function get isValidEmail():Boolean{
			var reg:RegExp = /^[a-z][\w.-]+@\w[\w.-]+\.[\w.-]*[a-z][a-z]$/i;
			return reg.test(_view.text);
		}
		
		public function reset():void{
			_view.text = _defaultCopy;
			_view.displayAsPassword = false;
		}
		
		public function showWarning(warningCopy:String="INVALID", milliseconds:Number=1000, colour:uint=0xD53C6B):void{
			_isWarningShowing = true;
			_warningCopy = warningCopy;
			_view.mouseEnabled = false;
			if(_view.textColor != colour)_colourBeforeWarning = _view.textColor;
			if(_view.text != warningCopy)_textBeforeWarning = _view.text;
			_view.text = warningCopy;
			_view.textColor = colour;
			_warningTimer = new Timer(milliseconds,1);
			_warningTimer.addEventListener(TimerEvent.TIMER, onTimer);
			_warningTimer.start();
		}
		
		private function onTimer(e:TimerEvent):void{
			_view.text = _textBeforeWarning;
			_view.textColor = _colourBeforeWarning;
			_view.mouseEnabled = true;
			_isWarningShowing = false;
		}

		public function get text():String{return _view.text;}
		public function set text(v:String):void{_view.text = v;}
		public function get view():TextField{return _view;}
	}
}
