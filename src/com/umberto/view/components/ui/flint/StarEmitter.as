package com.umberto.view.components.ui.flint {
	import org.flintparticles.common.actions.Age;
	import org.flintparticles.common.actions.Fade;
	import org.flintparticles.common.counters.Steady;
	import org.flintparticles.common.initializers.ColorInit;
	import org.flintparticles.common.initializers.Lifetime;
	import org.flintparticles.common.initializers.ScaleImageInit;
	import org.flintparticles.common.initializers.SharedImage;
	import org.flintparticles.twoD.actions.Friction;
	import org.flintparticles.twoD.actions.Move;
	import org.flintparticles.twoD.emitters.Emitter2D;
	import org.flintparticles.twoD.initializers.Position;
	import org.flintparticles.twoD.initializers.Velocity;
	import org.flintparticles.twoD.renderers.BitmapRenderer;
	import org.flintparticles.twoD.zones.DiscZone;
	import org.flintparticles.twoD.zones.RectangleZone;

	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;

  public class StarEmitter extends Emitter2D{
	public function StarEmitter(renderer:BitmapRenderer){
		counter = new Steady( 100 );
		var star:StarAsset = new StarAsset();
		var starBmd:BitmapData = new BitmapData(star.width, star.height,true,0xFFFFFF);
		starBmd.draw(star);
		  
//		var graphic:Bitmap = new Bitmap(new StarParticle(),"auto",true);
		var graphic:Bitmap = new Bitmap(starBmd,"auto",true);
//		addInitializer(new Position(new PointZone(new Point(-17.5,-17.5))));
//		addInitializer(new Position(new DiscZone( new Point( 0, 0 ), 100, 100 )));
		addInitializer(new Position(new RectangleZone(-50,-50,50,50)));
		addInitializer(new SharedImage(graphic));
//		addInitializer(new SetImageProperties({x:-17.5,y:-17.5}));
//		addInitializer( new AlphaInit( .8, .9 ) );
		addInitializer( new ColorInit( 0xFFD53C6B, 0xFFCCCCCC ) );
//		addInitializer( new Velocity( new DiscZone( new Point( 0, 0 ), 500, 400 ) ) );
		addInitializer( new Velocity( new DiscZone( new Point( 0, 0 ), 400, 350 ) ) );
		addInitializer( new Lifetime( 1, 3 ) );
		addInitializer(new ScaleImageInit(.2,.8));
//		addInitializer(new Rotation(-1,1));
		      
		addAction(new Age());
		addAction(new Move());
		addAction(new Friction(150));
		addAction(new Fade(.8,0));
//		addAction(new ScaleImage(2,3));
//		addAction(new Rotate());
		
//		addAction( new RotateToDirection() );
//		addAction( new RandomDrift( 15, 15 ) );

//		addActivity(new FollowMouse(renderer));
//		addActivity(new RotateEmitter(1));
      
    }
  }
}
