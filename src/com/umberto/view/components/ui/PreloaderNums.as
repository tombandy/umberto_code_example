package com.umberto.view.components.ui {
	import com.greensock.TweenLite;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;

	/**
	 * @author Tom
	 */
	public class PreloaderNums extends PreloaderAsset {
		
		public static const COMPLETE:String = "Preloader.Complete";
		
		private var _percent:Number = 0;
		
		public function PreloaderNums() {
			percentText.text = "0";
		}
		
		public function update(bytesLoaded:Number, bytesTotal:Number):void{
			var pct:Number = (bytesLoaded / bytesTotal) * 100;
			TweenLite.killTweensOf(this);
			TweenLite.to(this,1,{percent:pct,onComplete:onUpdateComplete});
		}
		
		public function set percent(v:Number):void{
			_percent = v;
			percentText.text = String(Math.round(_percent));
		}
		
		public function get percent():Number{
			return _percent;
		}
		
		private function onUpdateComplete():void{
			if(_percent == 100){
				TweenLite.killTweensOf(this);
				TweenLite.to(this,.3,{alpha:0,onComplete:onFadeComplete});
			}
		}
		
		private function onFadeComplete():void{
			dispatchEvent(new Event(COMPLETE));
		}
	}
}
