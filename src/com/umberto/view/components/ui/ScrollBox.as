package com.umberto.view.components.ui {

	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;

	/**
	 * @author tombandy
	 */
	public class ScrollBox {
		
		private var _content:Sprite;
		private var _contentMask:Sprite;
		private var _dragger:Sprite;
		private var _bar:Sprite;
		private var _mouseDragOffset:Point;
		
		public function ScrollBox(content:Sprite,contentMask:Sprite,dragger:Sprite,bar:Sprite) {
			_content = content;
			_contentMask = contentMask;
			_dragger = dragger;
			_bar = bar;
			
			_dragger.addEventListener(MouseEvent.MOUSE_DOWN, onDraggerDown);
			_dragger.buttonMode = true;
			_dragger.y = _bar.y;
			
			_content.y = _contentMask.y;
			
			_dragger.visible = 
			_bar.visible = _content.height > _contentMask.height;
		}
		
		private function onDraggerDown(e:MouseEvent):void{
			_mouseDragOffset = new Point(_dragger.mouseX,_dragger.mouseY);
			_dragger.stage.addEventListener(MouseEvent.MOUSE_UP, onDraggerUp);
			_dragger.stage.addEventListener(MouseEvent.MOUSE_MOVE, onDraggerMove);
		}
		
		private function onDraggerUp(e:MouseEvent):void{
			_dragger.stage.removeEventListener(MouseEvent.MOUSE_UP, onDraggerUp);
			_dragger.stage.removeEventListener(MouseEvent.MOUSE_MOVE, onDraggerMove);
			_mouseDragOffset = null;
		}
		
		private function onDraggerMove(e:MouseEvent):void{
			_dragger.y = _dragger.parent.mouseY - _mouseDragOffset.y;
			if(_dragger.y<_bar.y)_dragger.y=_bar.y;
			if(_dragger.y>_bar.y+(_bar.height-_dragger.height))_dragger.y=_bar.y+(_bar.height-_dragger.height);
			_content.y = Utils.map(_dragger.y,_bar.y,_bar.y+_bar.height-_dragger.height,_contentMask.y,_contentMask.y-(_content.height-_contentMask.height));
		}
		
		public function set visible(v:Boolean):void{
			_content.visible =
			_contentMask.visible =
			_dragger.visible =
			_bar.visible = v;
		}
		
		public function get visible():Boolean{
			return _content.visible;
		}
		
		public function reset():void{
			_dragger.y = _bar.y;
			_content.y = _contentMask.y;
			_dragger.visible = 
			_bar.visible = _content.height > _contentMask.height && _content.visible;
		}
		
		public function get isVisible():Boolean{
			return _bar.visible;
		}
	}
}
