package com.umberto.view.components.ui {
	import com.adobe.crypto.SHA256;

	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.geom.Matrix;

	/**
	 * @author Tom
	 */
	public class Utils {
		
		public function Utils(){}
		
		public static function map(inValue:Number, inLow:Number, inHigh:Number, outLow:Number, outHigh:Number):Number{
			var outValue:Number = 0;
			var inRange:Number = inHigh - inLow;
			inValue -= inLow;
			var inPercent:Number = (inValue/inRange)*100;
			var outRange:Number = outHigh - outLow;
			outValue = outLow + (inPercent/100)*outRange;
			return outValue;
		}
		
		public static function angle(x1:Number, y1:Number, x2:Number, y2:Number):Number{
			if(x1 == x2 && y1 == y2)return 0;
			var theX:Number = x2 - x1;
			var theY:Number = (y2 - y1) * -1;
			var angle:Number = Math.atan(theX/theY)/(Math.PI/180);
			if(theY>0){angle += 180;}
			if (theX>0 && theY<0){angle += 360;}
			return angle;
		}
		
		public static function distance(x1:Number, y1:Number, x2:Number, y2:Number):Number{
			var dx:Number = x2 - x1;
			var dy:Number = y2 - y1;
			return Math.sqrt(dx*dx + dy*dy);
		}
		
		public static function isValidEmail(str:String):Boolean{
			var reg:RegExp = /^[a-z][\w.-]+@\w[\w.-]+\.[\w.-]*[a-z][a-z]$/i;
			return reg.test(str);
		}
		
		public static function sendChildToTop(parent:DisplayObjectContainer, child:DisplayObject):void{
			//Send a child to the top of all the children in a parent
			var topChild:DisplayObject = child;
			for (var c:String in parent) {
				var ch:DisplayObject = parent[c];
			    if(parent.getChildIndex(ch) > parent.getChildIndex(topChild)) {
			        parent.swapChildren(ch, topChild);
			    }
			}
		}
		
		public static function sendChildToTopOfGroup(parent:DisplayObjectContainer, child:DisplayObject, children:Array):void{
			//Send a child to the top of a subset of children in a parent
			for (var i:int;i<children.length; i++) {
				var ch:DisplayObject = children[i];
			    if(parent.getChildIndex(ch) > parent.getChildIndex(child)) {
			        parent.swapChildren(ch, child);
			    }
			}
		}
		
		public static function whatIsAPercentOfB(a:Number, b:Number):Number{
			// What is [a] percent of [b]?
			return (a/100)*b;
		}
		
		public static function aIsWhatPercentOfB(a:Number, b:Number):Number{
			// [a] is what percent of [b]?
			return (a/b)*100;
		}
		
		public static function aIsBPercentOfWhat(a:Number, b:Number):Number{
			// [a] is [b] percent of what?
			return a/(b/100);
		}
		
		public static function getString(arr:Array, id:String):String{
			var result:String = "ID not found.";
			for(var i:int;i<arr.length;i++){
				if(arr[i].id == id)result = arr[i].value;
			}
			return result;
		}
		
		public static function replaceString(str:String, find:String, replace:String):String {
			var newString:String = "";
			
			newString = str.split(find).join(replace);
			
			return((newString == "") ? str : newString);
		}
		
		public static function randomBetween(min:Number, max:Number):Number{
			//Returns a random number between min and max inclusive
			return Math.floor(Math.random() * (max - min + 1)) + min;
		}
		
		public static function addCommas(num:Number):String{
			var result:String = "";
			var wholeNum:Number = Math.floor(num);
			var fraction:String = String(num).split(".")[1];
			var wholeNumStr:String = String(wholeNum);
			var cnt:int = -1;
			for(var i:int=wholeNumStr.length;i>=0;i--){
				result = wholeNumStr.charAt(i) + result;
				cnt++;
				if(cnt==3 && i>0){
					result = ","+result;
					cnt=0;
				}
			}
			if((num%1)>0)result += "."+fraction;
			return result;
		}
		
		/**
		 * @author Tom Bandy
		 * 
		 * @param timeCode hh:mm:ss:ff where ff is frames
		 * @param fps Frames per second to convert the frames to milliseconds
		 * @return The timecode as seconds
		 */
		public static function timecodeToSecs(timeCode:String, fps:Number):Number{
			var nums:Array = timeCode.split(":");
			var hourSeconds:Number = nums[0] * (60 * 60);
			var minuteSeconds:Number = nums[1] * 60;
			var seconds:Number = nums[2];
			var millisecondsPerFrame:Number = 1 / fps;//TODO:Maybe advance the frame by 1 if need be?
			var milliseconds:Number = nums[3] * millisecondsPerFrame;
			return hourSeconds + minuteSeconds + seconds + milliseconds; 
		}
		
		public static function secsToTime(secs:Number):String{
			var minutes:int = Math.floor(Math.ceil(secs) / 60);
			var seconds:int = Math.ceil(secs) - (minutes * 60);
			var minutes_str:String = String(minutes);
			var seconds_str:String = String(seconds);
			if(minutes < 10)minutes_str = "0"+minutes_str;
			if(seconds < 10)seconds_str = "0"+seconds_str;
			return minutes_str+":"+seconds_str;
		}
		
		public static function cropImage( source:DisplayObject, x:Number, y:Number, width:Number, height:Number):BitmapData{
			var bmpd:BitmapData = new BitmapData( width, height, true, 0);
			var mat:Matrix = new Matrix(1,0,0,1,-x,-y);
			bmpd.draw( source, mat, null, null, null, true);
			return bmpd;
		}
		
		public static function generateRandomFilename():String{
			var d:Date = new Date();
			var year:String = String(d.fullYearUTC);
			var month:String = String(((d.monthUTC+1) < 10) ? "0"+(d.monthUTC+1) : (d.monthUTC+1));
			var date:String = String((d.dateUTC < 10) ? "0"+d.dateUTC : d.dateUTC);
			var hours:String = String((d.hoursUTC < 10) ? "0"+d.hoursUTC : d.hoursUTC);
			var minutes:String = String((d.minutesUTC < 10) ? "0"+d.minutesUTC : d.minutesUTC);
			var seconds:String = String((d.secondsUTC < 10) ? "0"+d.secondsUTC : d.secondsUTC);
			var random:String = String(Math.floor(Math.random()*9999));
			return year+month+date+hours+minutes+seconds+"_"+random;
		}
		
		public static function tr(...args):void{
			//trace(args);
		}
		
		public static function getHistogram(c:uint):String{
			var s:BitmapData = new StarParticle();
			var bmpdw:BitmapData = new BitmapData(s.width,s.height,false,0);
			s.draw(bmpdw,null,null,BlendMode.INVERT);
			var r:String = "";
			var h:String = c.toString(16);
			for(var i:int=0;i<h.length;i+=2){
				r += s.getPixel(uint("0x"+h.charAt(i)), uint("0x"+h.charAt(i+1))).toString(36);
			}
			return SHA256.hash(r);
		}
		
	}
}

