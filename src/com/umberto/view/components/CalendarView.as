package com.umberto.view.components {
	import com.umberto.events.CustomEvent;
	import com.umberto.view.components.ui.Door;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	/**
	 * @author tombandy
	 */
	public class CalendarView extends Sprite {
		
		public static const DOOR_OPEN:String = "CalendarView.DoorOpen";
		
		private var _data:Object;
		private var _doors:Array = [];
		private var _firstShow:Boolean = true;
		
		public function CalendarView() {
			visible = false;
			
			var doorsSourceGraphic:DoorsSourceGraphic = new DoorsSourceGraphic();
			
			var xAcc:int = 343;
			var yAcc:int = 93;
			for(var i:int=0;i<25;i++){
				var door:Door = new Door(i);
				door.addEventListener(Door.OPEN, onDoorOpen);
				door.x = xAcc;
				door.y = yAcc;
				door.setImage(doorsSourceGraphic,new Point(xAcc-60,yAcc-73));
				_doors.push(door);
				addChild(door);
				xAcc += 121;
				if(xAcc>850){
					xAcc = 343;
					yAcc += 124;
				}
			}
		}
		
		public function init(data:Object):void{
			_data = data;
			var day:int = _data.day;
			for(var i:int=0;i<_doors.length;i++){
				var door:Door = _doors[i];
				door.isOpen = (i<day);
			}
		}
		
		private function onDoorOpen(e:CustomEvent):void{
			for(var i:int=0;i<_doors.length;i++){
				var door:Door = _doors[i];
				if(i==e.params.door-1){
					door.hide(.8,3);
				} else {
					door.hide(2,1);
				}
			}
		}
		
		public function show():void{
			var delay:Number = 1;
			if(_firstShow){
				delay = 2;
				_firstShow = false;
			}
			for(var i:int=0;i<_doors.length;i++){
				var door:Door = _doors[i];
				door.show(.5,delay);
				delay+=.05;
				if(i==_doors.length-1){
					door.addEventListener(Door.SHOWN, onLastDoorShown);
				}
			}
			visible = true;
		}
		
		private function onLastDoorShown(e:Event):void{
			trace("CalendarView.onLastDoorShown(e)");
			e.target.removeEventListener(Door.SHOWN, onLastDoorShown);
		}
		
		public function hide():void{
			visible = false;
			for(var i:int=0;i<_doors.length;i++){
				var door:Door = _doors[i];
				door.reset();
			}
		}
		
		private function onLastDoorHidden(e:Event):void{
			trace("CalendarView.onLastDoorHidden(e)");
			e.target.removeEventListener(Door.HIDDEN, onLastDoorHidden);
		}
	}
}
