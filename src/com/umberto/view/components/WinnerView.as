package com.umberto.view.components {
	import com.greensock.TweenLite;
	import com.greensock.easing.Linear;
	import com.umberto.events.CustomEvent;
	import com.umberto.model.vo.FormVO;
	import com.umberto.model.vo.PrizeVO;
	import com.umberto.view.components.ui.ScrollBox;
	import com.umberto.view.components.ui.SimpleButton;
	import com.umberto.view.components.ui.VerySimpleInputTextField;
	import com.umberto.view.components.ui.flint.TitleStarEmitter;

	import org.flintparticles.twoD.renderers.BitmapRenderer;

	import flash.display.Bitmap;
	import flash.display.BlendMode;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	/**
	 * @author tombandy
	 */
	public class WinnerView extends WinnerViewAsset {
		
		public static const BACK:String = "WinnerView.Back";
		public static const SEND:String = "WinnerView.Send";
		
		private var _data:PrizeVO;
		private var _image:Bitmap;
		
		private var _form:Sprite;
		private var _checkbox:MovieClip;
		private var _tncButton:Sprite;
		private var _nameText:VerySimpleInputTextField;
		private var _emailText:VerySimpleInputTextField;
		private var _address1Text:VerySimpleInputTextField;
		private var _address2Text:VerySimpleInputTextField;
		private var _townText:VerySimpleInputTextField;
		private var _postcodeText:VerySimpleInputTextField;
		private var _productDescriptionText:TextField;
		private var _scrollbar:ScrollBox;
		
		
		private var _flint : BitmapRenderer;
		private var _starEmitter:TitleStarEmitter;
		
		public function WinnerView() {
			visible = false;
			blendMode = BlendMode.LAYER;
			_form = getChildByName("form") as Sprite;
			var sendButton:MovieClip = _form.getChildByName("sendButton") as MovieClip;
			_checkbox = _form.getChildByName("checkbox") as MovieClip;
			_tncButton = _form.getChildByName("tncButton") as Sprite;
			_nameText = new VerySimpleInputTextField(_form.getChildByName("nameText") as TextField,"");
			_emailText = new VerySimpleInputTextField(_form.getChildByName("emailText") as TextField,"");
			_address1Text = new VerySimpleInputTextField(_form.getChildByName("address1Text") as TextField,"");
			_address2Text = new VerySimpleInputTextField(_form.getChildByName("address2Text") as TextField,"");
			_townText = new VerySimpleInputTextField(_form.getChildByName("townText") as TextField,"");
			_postcodeText = new VerySimpleInputTextField(_form.getChildByName("postcodeText") as TextField,"");
			_productDescriptionText = scrollbarContent.getChildByName("productDescriptionText") as TextField;
			_productDescriptionText.autoSize = TextFieldAutoSize.CENTER;
			
			var back:SimpleButton = new SimpleButton(backButton);
			backButton.addEventListener(MouseEvent.CLICK, onBackClick);
			var send:SimpleButton = new SimpleButton(sendButton);
			sendButton.addEventListener(MouseEvent.CLICK, onSendClick);
			_checkbox.gotoAndStop(2);
			_checkbox.buttonMode = true;
			_checkbox.addEventListener(MouseEvent.CLICK, onCheckboxClick);
			_tncButton.buttonMode = true;
			_tncButton.alpha = 0;
			_tncButton.addEventListener(MouseEvent.CLICK, onTncClick);
			
			thanks.visible = false;
			
			_flint = new BitmapRenderer(new Rectangle(0,0,flintBounds.width,flintBounds.height));
			_flint.mouseChildren = false;
			_flint.mouseEnabled = false;
			_starEmitter = new TitleStarEmitter();
			_starEmitter.x = flintBounds.width/2;
			_starEmitter.y = flintBounds.height/2;
			_flint.addEmitter(_starEmitter);
			flintBounds.addChild(_flint);
			
			_scrollbar = new ScrollBox(scrollbarContent, scrollbarMask, scrollbarDragger, scrollbarBar);
		}

		private function onBackClick(e:MouseEvent):void {
			dispatchEvent(new Event(BACK));
		}

		private function onSendClick(e:MouseEvent):void {
			trace("WinnerView.onSendClick(e)");
			var valid:Boolean = true;
			
			if(!_emailText.isValidEmail){
				_emailText.showWarning("Enter valid email");
				valid = false;
			}
			
			if(!_nameText.isFilledIn){
				_nameText.showWarning("Enter your name");
				valid = false;
			}
			
			if(!_address1Text.isFilledIn){
				_address1Text.showWarning("Enter your address");
				valid = false;
			}
			
			if(!_townText.isFilledIn){
				_townText.showWarning("Enter your town");
				valid = false;
			}
			
			if(!_postcodeText.isFilledIn){
				_postcodeText.showWarning("Invalid");
				valid = false;
			}
			
			if(valid){
				var formVO:FormVO = new FormVO();
				formVO.prizeId = _data.prizeId;
				formVO.name = _nameText.text;
				formVO.email = _emailText.text;
				formVO.address1 = _address1Text.text;
				formVO.address2 = _address2Text.text;
				formVO.town = _townText.text;
				formVO.postcode = _postcodeText.text;
				formVO.tnc = (_checkbox.currentFrame==2)?true:false;
				dispatchEvent(new CustomEvent(SEND, {"formVO":formVO}));
				form.visible = false;
				thanks.visible = true;
				_nameText.reset();
				_emailText.reset();
				_address1Text.reset();
				_address2Text.reset();
				_townText.reset();
				_postcodeText.reset();
				_checkbox.gotoAndStop(2);
			}
		}
		
		public function init(data:PrizeVO):void{
			_data = data;
			productTitleText.text = data.title.toUpperCase();
			var tf:TextFormat = new TextFormat();
			tf.bold = true;
			productTitleText.setTextFormat(tf);
			_productDescriptionText.text = data.description.toUpperCase();
			loadPicture();
			_scrollbar.reset();
			scrollShadow.visible = _scrollbar.isVisible;
		}
		
		public function show():void{
			alpha = 0;
			TweenLite.to(this,2,{autoAlpha:1,ease:Linear.easeNone});
			form.visible = true;
			thanks.visible = false;
			_starEmitter.start();
		}
		
		public function hide():void{
			TweenLite.to(this,1,{autoAlpha:0,ease:Linear.easeNone,onComplete:hideComplete});
		}
		
		private function hideComplete():void{
			_starEmitter.stop();
		}
		
		private function loadPicture():void{
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onPictureLoaded);
			loader.load(new URLRequest(_data.image));
			if(_image != null && imageHolder.contains(_image))imageHolder.removeChild(_image);
		}
		
		private function onPictureLoaded(e:Event):void{
			_image = LoaderInfo(e.target).content as Bitmap;
			_image.smoothing = true;
			_image.x = (255 - _image.width) / 2;
			_image.y = (255 - _image.height) / 2;
			imageHolder.addChild(_image);
		}
		
		private function onCheckboxClick(e:MouseEvent):void{
			_checkbox.gotoAndStop((_checkbox.currentFrame==1)?2:1);
		}
		
		private function onTncClick(e:MouseEvent):void{
			navigateToURL(new URLRequest("terms.html"),"_blank");
		}
	}
}
