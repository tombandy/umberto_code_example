package com.umberto.view.components {
	import com.greensock.TweenLite;
	import com.greensock.easing.Linear;
	import com.umberto.view.components.ui.flint.TitleStarEmitter;

	import org.flintparticles.twoD.renderers.BitmapRenderer;

	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.geom.Rectangle;
	/**
	 * @author tombandy
	 */
	public class BackgroundView extends BackgroundViewAsset {
		
		public static const SHOW_COMPLETE:String = "BackgroundView.ShowComplete";
		public static const FACEBOOK_SHARE_CLICK:String = "BackgroundView.FacebookShareClick";
		public static const TWITTER_SHARE_CLICK:String = "BackgroundView.TwitterShareClick";
		
		private var _flint : BitmapRenderer;
		private var _starEmitter:TitleStarEmitter;
		
		public function BackgroundView() {
			visible = false;
			title.alpha =
			subtitle.alpha =
			logo.alpha = 
			products.alpha = 
			shareText.alpha = 
			facebookShareButton.alpha =
			twitterShareButton.alpha = 0;
			facebookShareButton.buttonMode = true;
			facebookShareButton.addEventListener(MouseEvent.CLICK, onFacebookShareClick);
			twitterShareButton.buttonMode = true;
			twitterShareButton.addEventListener(MouseEvent.CLICK, onTwitterShareClick);
			
			_flint = new BitmapRenderer(new Rectangle(0,0,flintBounds.width,flintBounds.height));
			_flint.mouseChildren = false;
			_flint.mouseEnabled = false;
			_starEmitter = new TitleStarEmitter();
			_starEmitter.x = flintBounds.width/2;
			_starEmitter.y = flintBounds.height/2;
			_flint.addEmitter(_starEmitter);
			flintBounds.addChild(_flint);
		}
		
		public function show():void{
			visible = true;
			TweenLite.to(logo,1,{alpha:.68,ease:Linear.easeNone});
			TweenLite.to(title,.3,{alpha:1,ease:Linear.easeNone,delay:1});
			TweenLite.to(subtitle,.3,{alpha:1,ease:Linear.easeNone,delay:1.2});
			TweenLite.to(products,.3,{alpha:1,ease:Linear.easeNone,delay:1.4});
			TweenLite.to(shareText,.3,{alpha:1,ease:Linear.easeNone,delay:1.5});
			TweenLite.to(facebookShareButton,.3,{alpha:1,ease:Linear.easeNone,delay:1.5});
			TweenLite.to(twitterShareButton,.3,{alpha:1,ease:Linear.easeNone,delay:1.5,onComplete:onShowComplete});
		}
		
		public function hide():void{
			visible = false;
		}
		
		private function onShowComplete():void{
			dispatchEvent(new Event(SHOW_COMPLETE));
			_starEmitter.start();
		}
		
		private function onFacebookShareClick(e:MouseEvent):void{
			dispatchEvent(new Event(FACEBOOK_SHARE_CLICK));
			if(ExternalInterface.available)ExternalInterface.call("shareFacebook");
		}
		
		private function onTwitterShareClick(e:MouseEvent):void{
			dispatchEvent(new Event(TWITTER_SHARE_CLICK));
			if(ExternalInterface.available)ExternalInterface.call("shareTwitter");
		}
		
		public function fadeToCalendar():void{
			TweenLite.killTweensOf(logo);
			TweenLite.to(logo,1,{alpha:.68,ease:Linear.easeNone});
		}
		
		public function fadeToPrize():void{
			TweenLite.killTweensOf(logo);
			TweenLite.to(logo,1,{alpha:.15,ease:Linear.easeNone});
		}
	}
}
