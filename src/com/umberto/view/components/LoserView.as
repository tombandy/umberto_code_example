package com.umberto.view.components {
	import com.greensock.TweenLite;
	import com.greensock.easing.Linear;
	import com.umberto.model.vo.PrizeVO;
	import com.umberto.view.components.ui.ScrollBox;
	import com.umberto.view.components.ui.SimpleButton;
	import com.umberto.view.components.ui.flint.TitleStarEmitter;

	import org.flintparticles.twoD.renderers.BitmapRenderer;

	import flash.display.Bitmap;
	import flash.display.BlendMode;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	/**
	 * @author tombandy
	 */
	public class LoserView extends LoserViewAsset {
		
		public static const BACK:String = "LoserView.Back";
		
		private var _data:PrizeVO;
		private var _productDescriptionText:TextField;
		private var _scrollbar:ScrollBox;
		private var _facebookUrl:String;
		private var _twitterUrl:String;
		private var _showFacebookLinkThisTime:Boolean = (Math.random()<.5);
		private var _image:Bitmap;
		
		private var _flint : BitmapRenderer;
		private var _starEmitter:TitleStarEmitter;
		
		public function LoserView() {
			visible = false;
			blendMode = BlendMode.LAYER;
			var back:SimpleButton = new SimpleButton(backButton);
			backButton.addEventListener(MouseEvent.CLICK, onBackClick);
			facebookButton.buttonMode = true;
			facebookButton.addEventListener(MouseEvent.CLICK, onFacebookClick);
			facebookButton.visible = _showFacebookLinkThisTime;
			twitterButton.buttonMode = true;
			twitterButton.addEventListener(MouseEvent.CLICK, onTwitterClick);
			twitterButton.visible = !_showFacebookLinkThisTime;
			_productDescriptionText = scrollbarContent.getChildByName("productDescriptionText") as TextField;
			_productDescriptionText.autoSize = TextFieldAutoSize.CENTER;
			_scrollbar = new ScrollBox(scrollbarContent, scrollbarMask, scrollbarDragger, scrollbarBar);
			
			_flint = new BitmapRenderer(new Rectangle(0,0,flintBounds.width,flintBounds.height));
			_flint.mouseChildren = false;
			_flint.mouseEnabled = false;
			_starEmitter = new TitleStarEmitter();
			_starEmitter.x = flintBounds.width/2;
			_starEmitter.y = flintBounds.height/2;
			_flint.addEmitter(_starEmitter);
			flintBounds.addChild(_flint);
		}

		private function onBackClick(e:MouseEvent):void {
			dispatchEvent(new Event(BACK));
		}

		private function onTwitterClick(e:MouseEvent):void {
			navigateToURL(new URLRequest(_twitterUrl),"_blank");
		}

		private function onFacebookClick(e:MouseEvent):void {
			navigateToURL(new URLRequest(_facebookUrl),"_blank");
		}
		
		public function init(data:PrizeVO):void{
			_data = data;
			productTitleText.text = data.title.toUpperCase();
			var tf:TextFormat = new TextFormat();
			tf.bold = true;
			productTitleText.setTextFormat(tf);
			_productDescriptionText.text = data.description.toUpperCase();
			_scrollbar.reset();
			loadPicture();
			scrollShadow.visible = _scrollbar.isVisible;
		}
		
		private function loadPicture():void{
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onPictureLoaded);
			loader.load(new URLRequest(_data.image));
			if(_image != null && imageHolder.contains(_image))imageHolder.removeChild(_image);
		}
		
		private function onPictureLoaded(e:Event):void{
			_image = LoaderInfo(e.target).content as Bitmap;
			_image.smoothing = true;
			_image.x = (255 - _image.width) / 2;
			_image.y = (255 - _image.height) / 2;
			imageHolder.addChild(_image);
		}
		
		public function show():void{
			alpha = 0;
			TweenLite.to(this,2,{autoAlpha:1,ease:Linear.easeNone});
			_showFacebookLinkThisTime = !_showFacebookLinkThisTime;
			facebookButton.visible = _showFacebookLinkThisTime;
			twitterButton.visible = !_showFacebookLinkThisTime;
			_starEmitter.start();
		}
		
		public function hide():void{
			TweenLite.to(this,1,{autoAlpha:0,ease:Linear.easeNone,onComplete:hideComplete});
		}
		
		private function hideComplete():void{
			_starEmitter.stop();
		}
		
		public function set facebookUrl(v:String):void{
			_facebookUrl = v;
		}
		
		public function set twitterUrl(v:String):void{
			_twitterUrl = v;
		}
	}
}
