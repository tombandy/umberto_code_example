package com.umberto.view.components {
	import flash.filters.GlowFilter;
	import com.greensock.TweenLite;
	import com.greensock.easing.Linear;
	import com.umberto.view.components.ui.flint.StarEmitter;

	import org.flintparticles.twoD.renderers.BitmapRenderer;

	import flash.display.BlendMode;
	import flash.display.GradientType;
	import flash.display.InterpolationMethod;
	import flash.display.Shape;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	/**
	 * @author tombandy
	 */
	public class ParticleView extends Sprite {
		
		public static const FADE_IN_COMPLETE:String = "ParticleView.FadeInComplete";
		public static const FADE_OUT_COMPLETE:String = "ParticleView.FadeOutComplete";
		
		private var _flint : BitmapRenderer;
		private var _starEmitter:StarEmitter;
		private var _whiteOutCircle:Shape;
		private var _whiteBox:Shape;
		private var _darkBox:Shape;
		private var _bounds:Rectangle = new Rectangle(60, 73, 899, 647);
		
		public function ParticleView() {
			visible = false;
			_flint = new BitmapRenderer(_bounds);
			_flint.mouseChildren = false;
			_flint.mouseEnabled = false;
			mouseChildren = false;
			mouseEnabled = false;
//			_flint.addFilter( new BlurFilter( 4, 4, 1 ) );
//      	_flint.addFilter( new ColorMatrixFilter( [ 1,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0.95,0 ] ) );

			_starEmitter = new StarEmitter(_flint);
			_starEmitter.x = 1020/2;
			_starEmitter.y = 820/2;
			
			var whiteMask:Shape = new Shape();
			whiteMask.graphics.beginFill(0xFF6600);
			whiteMask.graphics.drawRect(0, 0, _bounds.width, _bounds.height);
			whiteMask.graphics.endFill();
			whiteMask.x = _bounds.x;
			whiteMask.y = _bounds.y;
			
			_darkBox = new Shape();
			
			_whiteBox = new Shape();
			_whiteBox.graphics.beginFill(0xFFFFFF);
			_whiteBox.graphics.drawRect(0, 0, _bounds.width, _bounds.height);
			_whiteBox.graphics.endFill();
			_whiteBox.x = _bounds.x;
			_whiteBox.y = _bounds.y;
			_whiteBox.blendMode = BlendMode.ADD;
			_whiteBox.alpha = 0;
			_whiteBox.filters = [new GlowFilter(0xFFFFFF,1,40,40,1.5,2)];
			
			var m:Matrix = new Matrix();
			m.createGradientBox(400, 400,0,-200,-200);
			_whiteOutCircle = new Shape();
			_whiteOutCircle.graphics.beginGradientFill(	GradientType.RADIAL, 
														[0xFFFFFF,0xFFFFFF], 
														[.3,0], 
														[128,255],
														m,
														SpreadMethod.PAD,
														InterpolationMethod.RGB,
														0);
			_whiteOutCircle.graphics.drawCircle(0, 0, 400);
			_whiteOutCircle.graphics.endFill();
			_whiteOutCircle.x = 1020/2;
			_whiteOutCircle.y = 820/2;
			_whiteOutCircle.cacheAsBitmap = true;
			_whiteOutCircle.mask = whiteMask;

			_flint.addEmitter(_starEmitter);
			
			addChild(_darkBox);
			addChild(whiteMask);
			addChild(_whiteOutCircle);
			addChild(_whiteBox);
			addChild(_flint);
		}
		
		public function show(doorBounds:Rectangle):void{
			visible = true;
			_darkBox.graphics.clear();
			_darkBox.graphics.beginFill(0x000000,.1);
			_darkBox.graphics.drawRect(0, 0, _bounds.width, _bounds.height);
			_darkBox.graphics.drawRect(doorBounds.x-_bounds.x, doorBounds.y-_bounds.y, doorBounds.width, doorBounds.height);
			_darkBox.graphics.endFill();
			_darkBox.x = _bounds.x;
			_darkBox.y = _bounds.y;
			_darkBox.alpha = 0;
			
			_starEmitter.x = doorBounds.x+(doorBounds.width/2);
			_starEmitter.y = doorBounds.y+(doorBounds.height/2);
			_whiteOutCircle.x = doorBounds.x+(doorBounds.width/2);
			_whiteOutCircle.y = doorBounds.y+(doorBounds.height/2);
			_whiteOutCircle.width = 
			_whiteOutCircle.height = 0;
			_whiteOutCircle.visible = true;
			TweenLite.to(_whiteOutCircle,2,{width:3000,height:3000,ease:Linear.easeNone,delay:.5,onComplete:removeCircle});
//			TweenLite.to(_whiteBox,1,{alpha:.6,ease:Linear.easeNone,delay:1.5,onComplete:removeCircle});
			TweenLite.to(_darkBox,1,{alpha:1,ease:Linear.easeNone,delay:0});
			TweenLite.to(_darkBox,.3,{alpha:0,ease:Linear.easeNone,delay:2});
			_starEmitter.start();
		}
		
		private function removeCircle():void{
			_whiteOutCircle.width = 
			_whiteOutCircle.height = 0;
			_whiteOutCircle.visible = false;
			_starEmitter.counter.stop();
			_darkBox.alpha = 0;
			dispatchEvent(new Event(FADE_IN_COMPLETE));
			TweenLite.to(_whiteBox,1,{alpha:0,ease:Linear.easeNone,onComplete:onFadeOutComplete});
		}
		
		private function onFadeOutComplete():void{
			visible = false;
			dispatchEvent(new Event(FADE_OUT_COMPLETE));
		}
		
		public function hide():void{
			visible = false;
		}
		
		public function back():void{
			visible = true;
//			_whiteBox.alpha = .6;
//			TweenLite.to(_whiteBox,1,{alpha:0,ease:Linear.easeNone,onComplete:onBackComplete});
		}
		
		private function onBackComplete():void{
			visible = false;
		}
	}
}
