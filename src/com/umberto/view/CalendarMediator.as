package com.umberto.view {
	import com.umberto.events.CmsServiceEvent;
	import com.umberto.events.CustomEvent;
	import com.umberto.remote.services.IConfigService;
	import com.umberto.view.components.CalendarView;
	import com.umberto.view.components.LoserView;
	import com.umberto.view.components.ParticleView;
	import com.umberto.view.components.WinnerView;
	import com.umberto.view.components.ui.Door;

	import org.robotlegs.mvcs.Mediator;

	import flash.events.Event;

	/**
	 * @author tombandy
	 */
	public class CalendarMediator extends Mediator {
		
		[Inject]
		public var view:CalendarView;
		
		[Inject]
		public var configService:IConfigService;
		
		public function CalendarMediator() {
		}
		
		override public function onRegister():void{
			addContextListener(CmsServiceEvent.SERVICE_STATUS, onStatus);
			addContextListener(WinnerView.BACK, onBack);
			addContextListener(LoserView.BACK, onBack);
			addContextListener(ParticleView.FADE_IN_COMPLETE, onHide);
			
			addViewListener(Door.OPEN, dispatch, CustomEvent);
			addViewListener(Door.OPENED, dispatch, CustomEvent);
		}
		
		private function onStatus(e:CmsServiceEvent) : void {
			view.init(e.data);
			view.show();
		}
		
		private function onBack(e:Event) : void {
			view.show();
		}
		
		private function onHide(e:Event) : void {
			view.hide();
		}
		
	}
}
