package com.umberto.view {
	import com.umberto.events.CmsServiceEvent;
	import com.umberto.events.CustomEvent;
	import com.umberto.model.IApplicationStateModel;
	import com.umberto.model.vo.PrizeVO;
	import com.umberto.remote.services.IConfigService;
	import com.umberto.view.components.ParticleView;
	import com.umberto.view.components.WinnerView;

	import org.robotlegs.mvcs.Mediator;

	import flash.events.Event;

	/**
	 * @author tombandy
	 */
	public class WinnerMediator extends Mediator {
		
		[Inject]
		public var view:WinnerView;
		
		[Inject]
		public var applicationStateModel:IApplicationStateModel;
		
		[Inject]
		public var configService:IConfigService;
		
		public function WinnerMediator() {
		}
		
		override public function onRegister():void{
			addContextListener(CmsServiceEvent.SERVICE_PRIZE, onPrize);
			addContextListener(ParticleView.FADE_IN_COMPLETE, onShowPrize);
			
			addViewListener(WinnerView.BACK, onBack);
			addViewListener(WinnerView.SEND, dispatch, CustomEvent);
		}
		
		private function onPrize(e:CmsServiceEvent) : void {
			if(e.data.win=="1")
				view.init(e.data as PrizeVO);
		}
		
		private function onShowPrize(e:Event) : void {
			if(applicationStateModel.currentPrize.win=="1")
				view.show();
		}
		
		private function onBack(e:Event):void{
			view.hide();
			dispatch(e.clone());
		}
		
	}
}
