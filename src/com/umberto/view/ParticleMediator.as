package com.umberto.view {
	import com.umberto.events.CustomEvent;
	import com.umberto.remote.services.IConfigService;
	import com.umberto.view.components.LoserView;
	import com.umberto.view.components.ParticleView;
	import com.umberto.view.components.WinnerView;
	import com.umberto.view.components.ui.Door;

	import org.robotlegs.mvcs.Mediator;

	import flash.events.Event;

	/**
	 * @author tombandy
	 */
	public class ParticleMediator extends Mediator {
		
		[Inject]
		public var view:ParticleView;
		
		[Inject]
		public var configService:IConfigService;
		
		public function ParticleMediator() {
		}
		
		override public function onRegister():void{
			addContextListener(Door.OPENED, onDoorOpen);
			addContextListener(WinnerView.BACK, onBack);
			addContextListener(LoserView.BACK, onBack);
			
			addViewListener(ParticleView.FADE_IN_COMPLETE, dispatch, Event);
			addViewListener(ParticleView.FADE_OUT_COMPLETE, dispatch, Event);
		}
		
		private function onDoorOpen(e:CustomEvent) : void {
			view.show(e.params.bounds);
		}
		
		private function onBack(e:Event):void{
			view.back();
		}
		
	}
}
