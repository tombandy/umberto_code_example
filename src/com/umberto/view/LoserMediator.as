package com.umberto.view {
	import com.umberto.events.ConfigServiceEvent;
	import com.umberto.model.IApplicationStateModel;
	import com.umberto.events.CmsServiceEvent;
	import com.umberto.model.vo.PrizeVO;
	import com.umberto.remote.services.IConfigService;
	import com.umberto.view.components.LoserView;
	import com.umberto.view.components.ParticleView;

	import org.robotlegs.mvcs.Mediator;

	import flash.events.Event;

	/**
	 * @author tombandy
	 */
	public class LoserMediator extends Mediator {
		
		[Inject]
		public var view:LoserView;
		
		[Inject]
		public var applicationStateModel:IApplicationStateModel;
		
		[Inject]
		public var configService:IConfigService;
		
		public function LoserMediator() {
		}
		
		override public function onRegister():void{
			addContextListener(ConfigServiceEvent.SERVICE_READY, onConfigReady);
			addContextListener(CmsServiceEvent.SERVICE_PRIZE, onPrize);
			addContextListener(ParticleView.FADE_IN_COMPLETE, onShowPrize);
			
			addViewListener(LoserView.BACK, onBack);
		}
		
		private function onConfigReady(e:ConfigServiceEvent) : void {
			view.facebookUrl = configService.getString("facebook_url");
			view.twitterUrl = configService.getString("twitter_url");
		}
		
		private function onPrize(e:CmsServiceEvent) : void {
			if(e.data.win=="0")
				view.init(e.data as PrizeVO);
		}
		
		private function onShowPrize(e:Event) : void {
			if(applicationStateModel.currentPrize.win=="0")
				view.show();
		}
		
		private function onBack(e:Event):void{
			view.hide();
			dispatch(e.clone());
		}
		
	}
}
