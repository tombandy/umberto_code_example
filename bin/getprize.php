<?php

$door = (isset($_REQUEST['door'])) ? strval($_REQUEST['door']) : false;

if ($door !== false) {

    switch($door){
        case "1":
        echo '{"status":1,"prize_id":"1","title":"Curl Friends Curl Creator Kit","description":"Our favourite look for curls – defined, frizz free loose curls with shine, movement and a superstar appeal! Create this look with this Get The Look kit including the mini Calm Curls Shampoo, Smooth Curls Conditioner, Flirty Curls Scrunching Jelly and a specially designed comb for curls!","image":"img/products/1st_studstripband.jpg","win":1,"buy_link":"http://www.boots.com/en/Umberto-Giannini-Mini-Curl-Creator-Hair_1273064/"}';
        break;
        case "2":
        echo '{"status":1,"prize_id":"2","title":"Incredible Body Big Volume Hair Kit","description":"Height, weight and fullness are the vital statistics for Incredible Body – if you don’t have it, fake it with this Incredible Get the Look kit! The kit includes the mini Volume Shampoo, Volume Conditioner and Volume Blow Dry Spray for Big Hair. There are also some fabulous rollers to create full-on glamorous locks.","image":"img/products/2nd_blackcrystalalice.jpg","win":1,"buy_link":"http://www.boots.com/en/Umberto-Giannini-Incredible-Body-Big-Hair-Volume-Styling-Kit_1273074/"}';
        break;
        case "3":
        echo '{"status":1,"prize_id":"3","title":"Sleek & Chic Glossy Blow Dry Kit","description":"Everything you need to create ladylike glamour with a cool girl edge! This Get the Look kit contains the mini Go Straight Shampoo, Keep It Straight Conditioner, Salon Straight Blow Dry Cream and a great brush to combat frizz!","image":"img/products/3rd_glamhairglamourisingessentials.jpg","win":0,"buy_link":"http://www.boots.com/en/Umberto-Giannini-Sleek-and-Chic-Glossy-Blow-Dry-Kit_1273066/"}';
        break;
        default:
        echo '{"status":0}';
        break;
    } 

} else {

    echo '{"status":0}';
    
} 

?>