<?php
/**
 * Project: Umberto
 * Feature: Return status and current day to calendar
 */
require_once '_includes/config.php';
require_once '_includes/connection.php';
require_once '_includes/functions.php';

$inTestMode = TEST_MODE; //set in config.php
$status = 0;
$day = 1;

//Get game status
if ($inTestMode) {
    $day = 25;
    $status = 1;
    
} else {
    $thisDay = intval(date('j'));
    $thisMonth = intval(date('n'));
    
    if ($thisMonth == 12 and $thisDay < 26) {
        $status = 1;
        $day = $thisDay;
    }
    
}

//$txt = $row[2];
//$txt = str_replace('"','',htmlspecialchars($txt)); //htmlspecialchars($row[2]);
//$txt = str_replace('\\','&#92;',$txt);
//$txt = str_replace("\n", '', $txt);

//header('Content-type: application/json');
//header('Expires: '.gmdate('D, d M Y H:i:s', time()+$cacheTime).'GMT');
//header('Cache-Control: public');


echo '{"status":'.$status.',"day":'.$day.'}';

?>