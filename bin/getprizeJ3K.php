<?php
/**
 * Project: Umberto
 * Feature: Return prize info
 */
require_once '_includes/config.php';
require_once '_includes/connection.php';
require_once '_includes/functions.php';

$inTestMode = TEST_MODE; //set in config.php
$status = 0;
$day = 1;

//Get game status
if ($inTestMode) {
    $day = 25;
    $status = 1;
    
} else {
    $thisDay = intval(date('j'));
    $thisMonth = intval(date('n'));
    
    if ($thisMonth == 12 and $thisDay < 26) {
        $status = 1;
        $day = $thisDay;
    }
    
}
$door = (isset($_REQUEST['door'])) ? intval($_REQUEST['door']) : false;

if ($door !== false) {
    if (!isset($_SESSION['userkey'])) {
        $userkey = generateKEY();
        $_SESSION['userkey'] = $userkey;
    } else {
        $userkey = $_SESSION['userkey'];
    }

    $sqlStr = 'SELECT var_title, var_desc, int_count, int_visitcount, int_claimcount, (SELECT int_prizerate FROM tbl_setting LIMIT 1) as rate FROM tbl_prize WHERE int_day = ?';
    $sth = $pdo->prepare($sqlStr);
    $sth->execute(array($door));
    $row = $sth->fetch(PDO::FETCH_NUM);
    
    $win = 0;
    $remainingCount = 0;
    $sDateUpdate = date('Y-m-d H:i:s');
    
    echo '{';
    
    if ($row) {
        $title = trim($row[0]);
        $desc = trim($row[1]);
        $prizeCount = intval($row[2]);
        $visitCount = intval($row[3]) + 1; //to include this visit
        $claimCount = intval($row[4]);
        $prizeRate = intval($row[5]);
        $remainingCount = $prizeCount - $claimCount;
    
        if ($door == intval(date('j')) or $inTestMode) {
            if ($visitCount % $prizeRate == 0 and $remainingCount > 0) {
                $win = 1;
                $sqlStr = 'INSERT INTO tbl_winner (int_day, var_key, date_createdate) VALUES (?, ?, ?)';
                $day = intval(date('j'));
                $sDateCreate = date('Y-m-d H:i:s');
                $sth = $pdo->prepare($sqlStr);
                $sth->execute(array($door, $userkey, $sDateCreate));

                $sqlStr = 'UPDATE tbl_prize SET int_claimcount = int_claimcount + 1, int_visitcount = int_visitcount + 1, date_updatedate = ? WHERE int_day = ?';

            } else {
                $sqlStr = 'UPDATE tbl_prize SET int_visitcount = int_visitcount + 1, date_updatedate = ? WHERE int_day = ?';

            }
            $sth = $pdo->prepare($sqlStr);
            $sth->execute(array($sDateUpdate, $door));
        }
        
        echo '"status":'.$status.',"prize_id":"'.$door.'","title":"'.$title.'","description":"'.$desc.'","image":"img/products/'.$door.'.jpg",';
        echo '"win":'.$win.',"buy_link":""';
        /*
        switch($door){
            case "1":
            echo '"status":1,"prize_id":"1","title":"Curl Friends Curl Creator Kit","description":"Our favourite look for curls – defined, frizz free loose curls with shine, movement and a superstar appeal! Create this look with this Get The Look kit including the mini Calm Curls Shampoo, Smooth Curls Conditioner, Flirty Curls Scrunching Jelly and a specially designed comb for curls!","image":"img/products/product1.jpg",';
            echo '"win":'.$win.',"buy_link":"http://www.boots.com/en/Umberto-Giannini-Mini-Curl-Creator-Hair_1273064/"';
            break;
            case "2":
            echo '"status":1,"prize_id":"2","title":"Incredible Body Big Volume Hair Kit","description":"Height, weight and fullness are the vital statistics for Incredible Body – if you don’t have it, fake it with this Incredible Get the Look kit! The kit includes the mini Volume Shampoo, Volume Conditioner and Volume Blow Dry Spray for Big Hair. There are also some fabulous rollers to create full-on glamorous locks.","image":"img/products/product2.jpg",';
            echo '"win":'.$win.',"buy_link":"http://www.boots.com/en/Umberto-Giannini-Incredible-Body-Big-Hair-Volume-Styling-Kit_1273074/"';
            break;
            case "3":
            echo '"status":1,"prize_id":"3","title":"Sleek & Chic Glossy Blow Dry Kit","description":"Everything you need to create ladylike glamour with a cool girl edge! This Get the Look kit contains the mini Go Straight Shampoo, Keep It Straight Conditioner, Salon Straight Blow Dry Cream and a great brush to combat frizz!","image":"img/products/product3.jpg",';
            echo '"win":'.$win.',"buy_link":"http://www.boots.com/en/Umberto-Giannini-Sleek-and-Chic-Glossy-Blow-Dry-Kit_1273066/"';
            break;
            default:
            echo '"status":0';
            break;
        } 
         */
        if ($inTestMode) {
            echo ',"ukey":"'.$userkey.'"';
        }
    } else {
        echo '"status":0';
    }
    echo '}';

} else {

    echo '{"status":0}';
    
} 

?>