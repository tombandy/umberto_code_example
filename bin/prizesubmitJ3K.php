<?php
/**
 * Project: Umberto
 * Feature: Submit prize claim
 */
require_once '_includes/config.php';
require_once '_includes/connection.php';
require_once '_includes/functions.php';

$globalSalt = CLIENT_GLOBALSALT;

//prizesubmitJ3K.php?prize_id=1&name=James%20B&email=mcberio@hotmail.com&address1=1%20Stuart%20St&address2=Shoreditch&town=London&postcode=RA%201PH&tnc=1&code=91dbc65e1beae167e47e1c0b4214557c

$prizeID = (isset($_REQUEST['prize_id'])) ? intval($_REQUEST['prize_id']) : false;
$name = (isset($_REQUEST['name'])) ? strval($_REQUEST['name']) : false;
$email = (isset($_REQUEST['email'])) ? strval($_REQUEST['email']) : false;
$addr1 = (isset($_REQUEST['address1'])) ? strval($_REQUEST['address1']) : false;
$addr2 = (isset($_REQUEST['address2'])) ? strval($_REQUEST['address2']) : false;
$town = (isset($_REQUEST['town'])) ? strval($_REQUEST['town']) : false;
$postcode = (isset($_REQUEST['postcode'])) ? strval($_REQUEST['postcode']) : false;
$tnc = (isset($_REQUEST['tnc'])) ? true : false;
$code = (isset($_REQUEST['code'])) ? strval($_REQUEST['code']) : false;

if ($prizeID !== false && $name !== false && $email !== false && $addr1 !== false && $addr2 !== false && $town !== false && $postcode !== false && $tnc !== false && $code !== false) {
    $codeCheck = md5($globalSalt.$email);
    //echo $codeCheck.'<br>';
    if ($code == $codeCheck and isset($_SESSION['userkey'])) {
        $userkey = $_SESSION['userkey'];
        $sqlStr = 'SELECT var_key FROM tbl_winner WHERE var_key = ?';
        $sth = $pdo->prepare($sqlStr);
        $sth->execute(array($userkey));
        $row = $sth->fetch(PDO::FETCH_NUM);
        
        $aesEmail = aes_encrypt($email);
        $aesPostcode = aes_encrypt($postcode);
        if ($row) {
            $sqlStr = 'UPDATE tbl_winner SET var_name = ?, var_email = ?, var_addr1 = ?, 
                        var_addr2 = ?, var_town = ?, var_postcode = ?, int_day = ?, int_prize = ?, 
                        date_createdate = ? WHERE var_key = ?';
            $day = intval(date('j'));
            $sDateCreate = date('Y-m-d H:i:s');
            $sth = $pdo->prepare($sqlStr);
            $sth->execute(array($name, $aesEmail, $addr1, $addr2, $town, $aesPostcode, $day, $prizeID, $sDateCreate, $userkey));
            
        } else {
            $sqlStr = 'INSERT INTO tbl_winner (var_name, var_email, var_addr1, var_addr2, var_town, var_postcode, int_day, int_prize, date_createdate) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)';
            $day = intval(date('j'));
            $sDateCreate = date('Y-m-d H:i:s');
            $sth = $pdo->prepare($sqlStr);
            $sth->execute(array($name, $aesEmail, $addr1, $addr2, $town, $aesPostcode, $day, $prizeID, $sDateCreate));
        }
    }
    echo '{"status":1}';

} else {

    echo '{"status":0}';
    
} 

?>