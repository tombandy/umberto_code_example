var ugUrl = 'http://www.umbertogianninichristmas.com/';
var ugUrlTxt = 'www.umbertogianninichristmas.com';
function shareTwitter() {
    var message = 'Open a door of the UG advent calendar every day until Xmas for the chance to win a gorgeously glam gift';
    window.open('https://twitter.com/intent/tweet?source=webclient&text='+encodeURIComponent(message)+'&url='+ugUrl,'_blank');
}
function shareFacebook() {
    var message = 'If you just can\'t wait until the big day, click to unlock a door of the UG online advent calendar every day from now until Christmas ';
    message += 'for the chance to win a gorgeously glam gift, from Darkly Glamorous Hair Decorations to a Luxury Hair Makeover session. www.umbertogianninichristmas.com';
    var fbURL = 'https://www.facebook.com/dialog/feed?app_id=456853991018835';
    fbURL += '&link='+ugUrl;
    fbURL += '&picture='+ugUrl+'img/fbninety.png';
    fbURL += '&name=Umberto%20Giannini';
    fbURL += '&caption=Umberto%20Giannini';
    fbURL += '&description='+encodeURIComponent(message);
    fbURL += '&redirect_uri='+ugUrl;
    window.open(fbURL, 'sharer', 'toolbar=0,status=0,width=980,height=595');
}
/*
function shareTwitter( message ){
    //alert( "ShareTwitter. message/" + message );
    window.open( 'http://twitter.com/?status=' + encodeURIComponent( message ), 'title','width=400,height=400' );
};
function shareFacebook( url, message ){
    //alert( "ShareFacebook. url/" + url + " message/" + message );
    window.open( 'http://www.facebook.com/sharer.php?u=' + encodeURIComponent( url ) + '&t=' + encodeURIComponent( message ), 'sharer', 'toolbar=0,status=0,width=980,height=595' );
};
*/