<?php
/**
 * Feature: Admin Screens - Calendar settings
 *
 */
require_once '../_includes/config.php';
require_once '../_includes/connection.php';
require_once '../_includes/functions.php';
isUserAuth(); //see functions.php - checks user logged in
$pagename = 'settings'; //used in inc_nav.php

if (isset($_REQUEST['action'])) {
    $prizerate = (isset($_REQUEST['prizerate'])) ? intval($_REQUEST['prizerate']) : 0;
    $sqlStr = 'UPDATE tbl_setting SET int_prizerate= ? WHERE int_setting_id=1';
    $sth = $pdo->prepare($sqlStr);
    $sth->execute(array($prizerate));
    
}

$sqlQuery = 'SELECT int_prizerate FROM tbl_setting WHERE int_setting_id=1 LIMIT 1';
$sth = $pdo->prepare($sqlQuery);
$sth->execute();

require_once 'inc_head.php';
?>
  <body>
<?php    
    require_once 'inc_nav.php';
?>
<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="span1"> &nbsp; </div>
            <div class="span10">
                <div class="page-header">
                    <h4>Prize settings</h4>
                </div>
            </div>
            <div class="span1"> &nbsp; </div>
        </div>   
        
        <div class="row">
            <div class="span1"> &nbsp; </div>
            <div class="span10">
                
            <form action="settings.php" method="post" name="heads" class="form-horizontal well">
            <fieldset><!-- legend> </legend -->
                <input type="hidden" name="action" value="update">
                <!-- nested -->
                <?php
                    $row = $sth->fetch(PDO::FETCH_NUM);
                    if ($row) {
                        $prizerate = $row[0];
                ?>

                        <div class="control-group">
                          <label class="control-label" style="width:120px" for="prizerate">Prize rate</label>
                          <div class="controls" style="margin-left: 140px">
                            <div class="input-prepend">    
                            <span class="add-on">1 in </span>   
                            <input type="text" name="prizerate" class="input-medium" value="<?php echo $prizerate; ?>" id="prizerate"></div>
                          </div>
                        </div>
     
                <?php
                    }
                ?>
        
            <div class="form-actions">
                <input type="hidden" name="action" value="update">
                <button type="submit" class="btn btn-small">Update</button>
            </div>
                
            </fieldset>
            </form>    
                
            <div class="span1"> &nbsp; </div>    
            </div>    
        </div>
        
    </div> <!-- /container -->
    </div> <!-- /wrapper -->
<?php    
    $pdo = null; //close db connection
    require_once 'inc_foot.php';
?>

  </body>
</html>