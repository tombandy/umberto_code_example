<div class="navbar navbar-fixed-top">
<div class="navbar-inner">
  <div class="container">
    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </a>
    <a class="brand" href="#">Umberto Giannini</a>
    <ul class="nav">
      <!-- li><a href="#">Home</a></li -->
      <li<?php if ($pagename == 'settings') { echo ' class="active"'; } ?>><a href="settings.php">Settings</a></li>
      <li<?php if ($pagename == 'prizes') { echo ' class="active"'; } ?>><a href="prizes.php">Prizes</a></li>
      <li<?php if ($pagename == 'winners') { echo ' class="active"'; } ?>><a href="winners.php">Winners</a></li>
    </ul>
    <ul class="nav pull-right">
        <li class="divider-vertical"></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user icon-white"></i> 
              <?php echo $_SESSION['login_admin_name']; ?> &nbsp; &nbsp; <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="index.php"><i class="icon-share"></i> Sign out</a></li>
          </ul>
        </li>
    </ul>
    <div class="nav-collapse">
    </div><!--/.nav-collapse -->
  </div>
</div>
</div>
