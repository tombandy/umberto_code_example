<?php
/** 
 * Feature: Admin Screens - Prizes
 *
 */
require_once '../_includes/config.php';
require_once '../_includes/connection.php';
require_once '../_includes/functions.php';
isUserAuth(); //see functions.php - checks user logged in
$pagename = 'winners';

require_once 'inc_head.php';
?>
  <body>
<?php    
    require_once 'inc_nav.php';
?>

    <div class="container">
        
        <div class="row">
            <div class="span1"> &nbsp; </div>
            <div class="span10">
                <table class="table table-striped table-condensed">
                  <tr><td colspan="5"> &nbsp; </td></tr>
                  <tr><td colspan="5"> &nbsp; </td></tr>
                    <tr>
                      <th>PrizeID</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Address</th>
                      <th>Time claimed</th>
                    </tr>
                  <tbody>
                  <?php
                    $page = isset($_REQUEST['pg']) ? intval($_REQUEST['pg']) : 0;
                    $perPage = 50;
                    
                    $sqlQuery = 'SELECT w.*, '.
                            '(SELECT count(int_winner_id) FROM tbl_winner) as total FROM tbl_winner w '.
                            'ORDER BY w.date_createdate DESC LIMIT :start, 50';
                    $iPage = $perPage*$page;
                    $sth = $pdo->prepare($sqlQuery);
                    $sth->bindParam(':start', $iPage, PDO::PARAM_INT);
                    $sth->execute();
                    //echo str_replace(':start', $iPage, $sqlQuery).'<hr>';
                    //$arr = $sth->errorInfo();
                    //print_r($arr);
                    
                    $total = 0;
                    $row = $sth->fetch(PDO::FETCH_ASSOC);
                    if ($row) {
                        $total = $row['total'];
                        $numOfPages = ceil($total/$perPage);
                        $currentPage = (($page+1)>$numOfPages) ? $numOfPages-1 : $page;
                        
                        $email = aes_decrypt($row['var_email']);
                        $postcode = aes_decrypt($row['var_postcode']);
                        $email = ($email != '') ? '<a href="mailto:'.$email.'">'.$email.'</a>' : '';
                        $name = trim($row['var_name']);
                        $addr = (strval($name) != '') ? $row['var_addr1'].', '.$row['var_addr2'].', '.$row['var_town'].', '.$postcode : '';
                        $name = (strval($name) != '') ? $name : '- unclaimed -';
                        echo '<tr><td>'.$row['int_day'].'</td>';
                        echo '<td>'.$name.'</td><td>'.$email.'</td>';
                        echo '<td>'.$addr.'</td><td>'.$row['date_createdate'].'</td></tr>'."\n";

                        while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
                            $email = aes_decrypt($row['var_email']);
                            $postcode = aes_decrypt($row['var_postcode']);
                            $email = ($email != '') ? '<a href="mailto:'.$email.'">'.$email.'</a>' : '';
                            $name = trim($row['var_name']);
                            $addr = (strval($name) != '') ? $row['var_addr1'].', '.$row['var_addr2'].', '.$row['var_town'].', '.$postcode : '';
                            $name = (strval($name) != '') ? $name : '- unclaimed -';
                            echo '<tr><td>'.$row['int_day'].'</td>';
                            echo '<td>'.$name.'</td><td>'.$email.'</td>';
                            echo '<td>'.$addr.'</td><td>'.$row['date_createdate'].'</td></tr>'."\n";
                        }
                    } else {
                        echo '<tr><td colspan="5">no data</td></tr>';
                    }
                    $pdo = null; //close db connection
                  ?>
                  </tbody>
                </table>
              </div>
            <div class="span1"> &nbsp; </div>
        </div>
        <div class="row">
            <div class="span1"> &nbsp; </div>
            <div class="span10">
                <div class="pagination">
                <ul>
                <?php
                    if ($total>$perPage) {
                        if ($currentPage>0) {
                                echo '<li><a href="winners.php?pg='.($currentPage-1).'">&lt; Previous</a></li>';
                        } else {
                                echo '<li class="disabled"><a href="#">&lt; Previous</a></li>';
                        }
                        if (($currentPage+1)<$numOfPages) {
                                echo '<li><a href="winners.php?pg='.($currentPage+1).'">Next &gt;</a></li>';
                        } else {
                                echo '<li class="disabled"><a href="#">Next &gt;</a></li>';
                        }
                        
                    }      
                ?>
                </ul>
                </div>
            </div>     
            <div class="span1"> &nbsp; </div>
        </div>
    </div> <!-- /container -->
<?php    
    require_once 'inc_foot.php';
?>

  </body>
</html>