<?php
/**
 * Feature: Admin Screens Login
 *
 */
require_once '../_includes/config.php';
require_once '../_includes/connection.php';
require_once '../_includes/functions.php';
require_once '../_includes/recaptchalib.php';

$publickey = RECAPTCHA_PUBLICKEY; //set in config.php
$privatekey = RECAPTCHA_PRIVATEKEY;
$resp = null; // the response from reCAPTCHA
$error =  null; // the error code from reCAPTCHA, if any
$globalSalt = CRYPT_GLOBALSALT; //set in config.php

if (isset($_POST['action'])) {
    $resp = recaptcha_check_answer(
        $privatekey, $_SERVER["REMOTE_ADDR"], 
        $_POST["recaptcha_challenge_field"],
        $_POST["recaptcha_response_field"]
    );
    $token = isset($_POST['token']) ? trim($_POST['token']) : '';
    $user = isset($_POST['username']) ? trim($_POST['username']) : false;
    $pass = isset($_POST['password']) ? trim($_POST['password']) : false;
    if ($resp->is_valid== true and $token!='' and $user !== false and $pass !== false and $token==$_SESSION['csrfToken']) {
        //continue login
        $sqlQuery = 'SELECT var_hash, var_uhash, int_admin_id, var_name FROM tbl_admin WHERE var_email=? LIMIT 1';
        $sth = $pdo->prepare($sqlQuery);
        $sth->execute(array($user));
        $row = $sth->fetch(PDO::FETCH_NUM);
        if ($row) {
            $hash = trim($row[0]);
            $uhash = trim($row[1]);
            $adminID = intval($row[2]);
            $adminName = trim($row[3]);
            $pdo = null; //close db connection
            
            $hashOK = check_password($hash, $pass, $globalSalt, $uhash);
            if ($hashOK !== false) {
                $_SESSION['login_admin'] = true;
                $_SESSION['login_admin_id'] = $adminID;
                $_SESSION['login_admin_name'] = $adminName;
                session_regenerate_id(); //prevent session fixation attacks
                //redirect to first screen
                header("location:settings.php");
                exit;
            }
        } else {
            //not found
            $pdo = null; //close db connection
        }  
        
    } else {
        $error = $resp->error;
    }
} else {
    session_unset();
    //session_destroy();
    //session_write_close();
    session_regenerate_id(true); //prevent session fixation attacks
}
//To prevent Cross site request forgery (CSRF)
$csrf = md5(uniqid(rand(), true)).uniqid(rand(), true);
$_SESSION['csrfToken'] = $csrf;

require_once 'inc_head.php';

?>
  <body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">Umberto Giannini</a>
          
          <!-- div class="nav-collapse">
            <ul class="nav">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#about">About</a></li>
              <li><a href="#contact">Contact</a></li>
            </ul>
          </div --><!--/.nav-collapse -->
        </div>
      </div>
    </div>
    <div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="span8 offset2">
                
            <form action="index.php" method="post" name="login" class="form-horizontal">
              <fieldset>
                <legend>Login</legend>
                <div class="control-group">
                  <label class="control-label" for="username">Username</label>
                  <div class="controls">
                    <input type="text" class="input-xlarge" name="username" id="username" autocomplete="off">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="password">Password</label>
                  <div class="controls">
                    <input type="password" name="password" class="input-xlarge" id="password" autocomplete="off">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Verification</label>
                  <div class="controls">
                  <?php
                  echo recaptcha_get_html($publickey, $error);
                  ?>
                  </div>
                </div>
                <div class="form-actions">
					<input type="hidden" name="action" value="login">
					<input type="hidden" name="token" value="<?php echo $csrf; ?>">
					<button type="submit" class="btn btn-primary">Login</button>
                </div>
              </fieldset>
            </form>
               
            </div>
        </div>
      
    </div> <!-- /container -->
    </div> <!-- /wrapper -->
<?php    
    //require_once 'inc_foot.php';
?>
  </body>
</html>