<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Umberto Giannini</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Uses Twitter Bootstrap -->
  <link href="css/bootstrap-wysihtml5-0.0.2.css" rel="stylesheet">
  <link href="css/bootstrap-silver.min.css" rel="stylesheet">
  <link href="css/datepicker.css" rel="stylesheet">
  <style>
    html, body { height: 100%; }  
    .wrapper { min-height: 100%; height: auto !important; height: 100%; margin: 0 auto -63px; }
    .push { height: 63px; }
    body { padding-top: 0 }
    .wrapper .container { padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */ }
    label, span { font-size: 0.9em }
    .footer { background-color: #F5F5F5; border-top: 1px solid #E5E5E5; margin-top: 50px; padding: 50px 0px; font-size: 90%; }
  </style>
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
  <script>
  var RecaptchaOptions = {
     theme : 'white'
  };
  </script>
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <!-- Fav and touch icons -->
  <!-- link rel="shortcut icon" href="ico/favicon.ico">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png" -->
</head>