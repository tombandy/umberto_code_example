<?php
/** 
 * Feature: Admin Screens - Prizes
 *
 */
require_once '../_includes/config.php';
require_once '../_includes/connection.php';
require_once '../_includes/functions.php';
isUserAuth(); //see functions.php - checks user logged in
$pagename = 'prizes';

if (isset($_REQUEST['action'])) {
    
    $prizeID = isset($_REQUEST['submit']) ? intval($_REQUEST['submit']) : 0;
    if ($prizeID > 0) {
        $count = isset($_REQUEST['p'.$prizeID]) ? intval($_REQUEST['p'.$prizeID]) : 0;
        
        $sqlStr = 'UPDATE tbl_prize SET int_count = ? WHERE int_prize_id = ?';
        echo 'UPDATE tbl_prize SET int_count = '.$count.' WHERE int_prize_id = '.$prizeID;
        $sth = $pdo->prepare($sqlStr);
        $sth->execute(array($count, $prizeID));
    }
}    

require_once 'inc_head.php';
?>
  <body>
<?php    
    require_once 'inc_nav.php';
?>

    <div class="container">
        
        <div class="row">
            <div class="span1"> &nbsp; </div>
            <div class="span10">
                
                <form action="prizes.php" method="post" name="prizes" class="form-horizontal well">
                <fieldset><!-- legend> </legend -->
                    <input type="hidden" name="action" value="update">
                
                <table class="table table-striped">
                  <!-- thead -->
                  <tr><td colspan="5"> &nbsp; </td></tr>
                    <tr>
                      <th>PrizeID</th>
                      <th width="50%">Title</th>
                      <th>Visits</th>
                      <th>Claims</th>
                      <th>Total Prizes</th>
                      <th>Update</th>
                    </tr>
                  <!-- /thead -->
                  <tbody>
                  <?php
                    
                    $sqlQuery = 'SELECT int_prize_id,var_title,int_count,int_visitcount,int_claimcount FROM tbl_prize ORDER BY int_prize_id ASC';
                    $sth = $pdo->prepare($sqlQuery);
                    $sth->execute();
                    
                    $total = 0;
                    $row = $sth->fetch(PDO::FETCH_ASSOC);
                    if ($row) {             
                        $pID = intval($row['int_prize_id']);
                        echo '<tr><td>'.$pID.'</td>';
                        echo '<td>'.$row['var_title'].'</td><td>'.$row['int_visitcount'].'</td>';
                        echo '<td>'.$row['int_claimcount'].'</td><td><input type="text" name="p'.$pID.'" class="input-mini" value="'.$row['int_count'].'"></td>';
                        echo '<td><button type="submit" name="submit" value="'.$pID.'" class="btn btn-mini">Save</button></td></tr>'."\n";

                        while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
                            $pID = intval($row['int_prize_id']);
                            echo '<tr><td>'.$pID.'</td>';
                            echo '<td>'.$row['var_title'].'</td><td>'.$row['int_visitcount'].'</td>';
                            echo '<td>'.$row['int_claimcount'].'</td><td><input type="text" name="p'.$pID.'" class="input-mini" value="'.$row['int_count'].'"></td>';
                            echo '<td><button type="submit" name="submit" value="'.$pID.'" class="btn btn-mini">Save</button></td></tr>'."\n";
                        }
                    } else {
                        echo '<tr><td colspan="5">no data</td></tr>';
                    }
                    $pdo = null; //close db connection
                  ?>
                  </tbody>
                </table>
                    
                </fieldset>
                </form>  
                
              </div>
            <div class="span1"> &nbsp; </div>
        </div>
        <div class="row">
            <div class="span1"> &nbsp; </div>
            <div class="span10">
                
            </div>     
            <div class="span1"> &nbsp; </div>
        </div>
    </div> <!-- /container -->
<?php    
    require_once 'inc_foot.php';
?>

  </body>
</html>