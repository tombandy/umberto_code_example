
<footer class="footer">
  <div class="container">
    <p class="pull-right"><a href="#">Back to top</a></p>
    <p>Developed by <a href="http://ralphandco.com/">Ralph</a></p>
  </div>
</footer>
<!-- Placed at the end for faster page load -->
<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-wysihtml5-0.0.2.min.js"></script>
<script>
$(document).ready(function(){
   //$("[rel=tooltip]").tooltip({placement:'top'});
});
</script>