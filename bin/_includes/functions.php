<?php
/**
 * Project: Umberto
 * Feature: App utility functions
 *
 */
function get_ipaddress() {
    if (isset($_SERVER)) {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
    } else {
        if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('HTTP_CLIENT_IP')) {
            $ip = getenv('HTTP_CLIENT_IP');
        } else {
            $ip = getenv('REMOTE_ADDR');
        }
    }
    return $ip;
}
function check_email_address($email) {
    // First, we check that there's one @ symbol, and that the lengths are right
    if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
        // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
        return false;  
    }
    // Split it into sections to make life easier
    $email_array = explode("@", $email);
    $local_array = explode(".", $email_array[0]);
    for ($i = 0; $i < sizeof($local_array); $i++) {
        if (!ereg("^(([A-Za-z0-9!#$%&amp;'*+/=?^_`{|}~-][A-Za-z0-9!#$%&amp;'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
            return false;
        }
    }
    if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
        $domain_array = explode(".", $email_array[1]);
        if (sizeof($domain_array) < 2) {
            return false; // Not enough parts to domain
        }
        for ($i = 0; $i < sizeof($domain_array); $i++) {
            if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
                return false;
            }
        }
    }
    return true;
}
function generateKEY($length = 28) {
    $skey = "";
    $possible = "0123456789abcdefghjkmnpqrstuvwxyz";
    $i = 0;
    if ($length < 6) { 
        $length = 6; 
    }
    while ($i < $length) {
        $char = substr($possible, mt_rand(0, strlen($possible)-1), 1); //pick random char from possible
        if (!strstr($skey, $char)) { //don't use if its already in key
            $skey .= $char;
            $i++;
        }
    }
    $postkey = strtolower(date('M'));
    return $skey.$postkey;
}
function hash_password($password, $globalSalt, &$userSalt) {
    //hash and userSalt are stored in DB, globalSalt is in config, password is not stored anywhere
    //note: userSalt is passed by ref - comes in as empty string and value is set below
    $algo = (CRYPT_BLOWFISH == 1) ? '$2a$07$' : '$1$';//set hash type to Blowfish or MD5 
    $userSalt = substr(md5(rand(1000000,9999999).str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz').rand(1000000,9999999)), 0, 22);
    $hash = crypt($password, $algo.$userSalt.$globalSalt);
    if ($algo == '$2a$07$') {
        $hash = substr($hash, 7); //remove blowfish algo from hash
    } else {
        $hash = substr($hash, 3); //remove md5 algo from hash
    }
    return $hash;
}
function check_password($hash, $password, $globalSalt, $userSalt) {
    //hash and userSalt are stored in DB, globalSalt is in config, password is not stored anywhere
    $algo = (CRYPT_BLOWFISH == 1) ? '$2a$07$' : '$1$';//set hash type to Blowfish or MD5 
    $newHash = crypt($password, $algo.$userSalt.$globalSalt);
    $checkResult = false;
    if ($algo == '$2a$07$' and '$2a$07$'.$hash == $newHash) {
            $checkResult = true;
    } elseif ($algo == '$1$' and '$1$'.$hash == $newHash) {
            $checkResult = true;
    }
    return $checkResult;  
}
function makesafe( $string ) {
    //to help prevent sql-injection attacks etc
    return mysqli_real_escape_string($mysqlID, $string); //$mysqlID set in connection.php
}
function cleaninput($input) {
    //to help stop XSS attacks
    $search = array(
        '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
        '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
        '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
        '@<![\s\S]*?--[ \t\n\r]*>@',        // Strip multi-line comments
        '@;|=|#|\*|--|mysql|sysobjects|syscolumns|INSERT INTO\b|DELETE FROM\b|TRUNCATE TABLE\b|DROP DATABASE\b|DROP TABLE\b@i' //Strip SQL terms
     );
    $output = preg_replace($search, '', $input);
    $output = substr($output, 0, 250); //max len 250 chars
    return $output;
}
function cleaninput_admin($input) {
        //to help stop XSS attacks - admin version must allow wysiwyg content
        $input = preg_replace('@&nbsp;@','&nbsp',$input);
        $search = array(
            '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
            '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
            '@<![\s\S]*?--[ \t\n\r]*>@',        // Strip multi-line comments
            '@;|\*|--|mysql|sysobjects|syscolumns|INSERT INTO\b|DELETE FROM\b|TRUNCATE TABLE\b|DROP DATABASE\b|DROP TABLE\b@i' //Strip SQL terms
         );
        $input = preg_replace($search, '', $input);
        $output = preg_replace('@&nbsp@','&nbsp;',$input);
        return $output;
}
function post_https($url, $post_data) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL , $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1);
        curl_setopt($ch, CURLOPT_TIMEOUT , 60);
        curl_setopt($ch, CURLOPT_ENCODING, "UTF-8");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_USERAGENT , "" );
        curl_setopt($ch, CURLOPT_POST , 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS , $post_data );

        $response = curl_exec($ch);

        if (curl_errno($ch)) {
                $error_message = curl_error($ch);
                $error_no = curl_errno($ch);

                echo "<!-- error_message: " . $error_message . " ";
                echo "error_no: " . $error_no . " -->";
        }

        curl_close($ch);

        return $response;
}

function curl_request_async($url, $params, $type='GET') {
    //open socket, make request, and immediately close without waiting for response
    foreach ($params as $key => &$val) {
        if (is_array($val)) $val = implode(',', $val);
        $post_params[] = $key.'='.urlencode($val);
    }
    $post_string = implode('&', $post_params);
    $parts=parse_url($url);
    $port = isset($parts['port']) ? $parts['port'] : 80;

    $fp = fsockopen($parts['host'], $port, $errno, $errstr, 30);
    if (!$fp) {
        //error
    } else {
        // Data goes in the path for a GET request
        if('GET' == $type) $parts['path'] .= '?'.$post_string;

        //echo '<!-- ASYNC '.$parts['host'].$parts['path'].' -->'."\n";

        $out = "$type ".$parts['path']." HTTP/1.1\r\n";
        $out.= "Host: ".$parts['host']."\r\n";
        $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
        $out.= "Content-Length: ".strlen($post_string)."\r\n";
        $out.= "Connection: Close\r\n\r\n";
        // Data goes in the request body for a POST request
        if ('POST' == $type and isset($post_string)) $out.= $post_string;

        $fwrite = fwrite($fp, $out);
        stream_set_timeout($fp, 1);
        /*
        if ($fwrite === false) {
            echo '<!-- ASYNC fwrite err -->'."\n";
        } else {
            echo '<!-- ASYNC fwrite ok -->'."\n";
        }
        echo '<!-- ASYNC '; 
        echo fread($fp, 2000);
        echo ' -->'."\n";
         */
        fclose($fp);
    }
}
function array_idx($array, $key, $default=null) {
    return array_key_exists($key, $array) ? $array[$key] : $default;
}
function base64_url_decode($input) {
    return base64_decode(strtr($input, '-_', '+/'));
}
function timeDiff($firstTime, $lastTime) {
    $firstTime=strtotime($firstTime);
    $lastTime=strtotime($lastTime);
    // perform subtraction to get the difference (in seconds) between times
    $timeDiff=$lastTime-$firstTime;
    return $timeDiff;
}
function isUserAuth() {
    if ($_SESSION['login_admin']) {
        if (!$_SESSION['login_admin_id'] > 0) {
            header("location:index.php"); //echo "admin_id is not greater then 0";
            exit;
        }
    } else {
        header("location:index.php"); //echo "admin session is not set";
        exit;
    }
}
function mysql_aes_key($key) {
    $new_key = str_repeat(chr(0), 16);
    for($i=0,$len=strlen($key);$i<$len;$i++)
    {
            $new_key[$i%16] = $new_key[$i%16] ^ $key[$i];
    }
    return $new_key;
}
function aes_encrypt($val) {
	$key = mysql_aes_key('e9c2f8v843nG55KzKts3ARVwV9RF8r1Dm6760s92cu1Fj34');
	$pad_value = 16-(strlen($val) % 16);
	$val = str_pad($val, (16*(floor(strlen($val) / 16)+1)), chr($pad_value));
	return mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $val, MCRYPT_MODE_ECB, mcrypt_create_iv( mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_DEV_URANDOM));
}
function aes_decrypt($val) {
	if ($val == '') {
		return $val;
	}	
	$key = mysql_aes_key('e9c2f8v843nG55KzKts3ARVwV9RF8r1Dm6760s92cu1Fj34');
	$val = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $val, MCRYPT_MODE_ECB, mcrypt_create_iv( mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_DEV_URANDOM));
	return rtrim($val, "\0..\16");
}
?>