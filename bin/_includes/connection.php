<?php
/**
 * Project: Umberto
 * Feature: DB connection
 *
 */
try {
    $dbHost = DB_HOST;
    $dbName = DB_DATABASE;
    $pdo = new PDO("mysql:host=$dbHost;dbname=$dbName;charset=utf8", DB_USERNAME, DB_PASSWORD);
    //$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {	
    //echo $e->getMessage();
    //die('Connection unavailable');
}    
session_start();
?>